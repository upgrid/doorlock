class Tag(object):
    def __init__(self, id=int(), uid=""):
        self.id = id
        self.uid = uid


class IdCard(object):
    def __init__(self, id=int(), cardNr="", idCode=""):
        self.id = id
        self.cardNr = cardNr
        self.idCode = idCode


class PersonAuthData(object):
    def __init__(
        self,
        personName="",
        idCode="",
        tagUid="",
        idCardNr="",
        tagId=int(),
        idCardId=int(),
        personId=int(),
        active=bool(),
        unlimitGroup=bool(),
    ):
        self.name = personName
        self.id = personId
        self.idCard = IdCard(idCardId, idCardNr, idCode)
        self.tag = Tag(tagId, tagUid)
        self.active = active
        self.unlimitedGroup = unlimitGroup

    def set_person_name(self, name):
        if name:
            self.name = name
        else:
            self.name = ""
