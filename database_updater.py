import json
import logging
import sqlite3
import urllib.request
from time import time

from watchdog.events import FileSystemEventHandler


class DatabaseUpdater(FileSystemEventHandler):
    def __init__(self, app):
        """
        :type app: LockApplicationMain
        """
        self.app = app
        super().__init__()
        self.idCards = []
        self.persons = []
        self.tags = []
        self.groupTimelimits = []
        self.delGroupTimelimits = []
        self.addPersonToGroup = []
        self.delPersonFromGroup = []
        self.lastUpdate = int(time() * 1000)
        self.syncFrom = int(time() * 1000)
        self.databaseSyncErrors = []

    def on_modified(self, event):
        if event.src_path == self.app.settings.UPDATE_MARKER_FILE:
            self.handleStateMotification()

    def handleStateMotification(self):
        if self.check_status():
            self.update_database()

    def clear_insertion_lists(self):
        self.idCards = []
        self.persons = []
        self.tags = []
        self.delPersons = []
        self.delTags = []
        self.delIdCards = []
        self.groups = []
        self.delGroups = []
        self.addPersonToGroup = []
        self.delPersonFromGroup = []

    def clear_group_timelimits_insertion_lists(self):
        self.groupTimelimits = []
        self.delGroupTimelimits = []

    def setLastUpdate(self):
        self.lastUpdate = int(time() * 1000)

    def calculateSyncFromShift(self):
        return self.syncFrom - self.app.settings.EMPTY_LIST_SYNCROM_SHIFT * 60 * 1000

    def update_database(self, syncFromShift=0):
        self.setLastUpdate()
        request = urllib.request.Request(
            url=self.app.settings.SYNC_URL
            + "sync/tagsandgrouptimelimits/"
            + str(self.syncFrom),
            headers={
                "Accept": "application/json, text/plain, */*",
                "X-Access-Token": self.app.settings.TOKEN,
                "Content-Type": "application/json; charset=utf-8",
            },
            method="GET",
        )

        try:
            with urllib.request.urlopen(request) as response_bytes:
                response = json.loads(response_bytes.read().decode("utf-8"))
                if response["result"] == "OK":
                    if self.add_to_database(response["data"], syncFromShift):
                        self.syncFinished()
                else:
                    raise Exception("Sync failed, server side issue.")

        except Exception as e:
            self.clearMarker()
            logging.exception("Error durning database sync: " + str(e))

    def insert_person(self):
        self.c.executemany(
            """INSERT OR REPLACE INTO main.users_person(id, name, added, updated, active) VALUES(?, '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1)""",
            self.persons,
        )

    def insert_tag(self):
        self.c.executemany(
            """INSERT OR REPLACE INTO main.users_tag(id, person_id, uid, active, added, updated) VALUES(?, ?, ?, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)""",
            self.tags,
        )

    def insert_idcard(self):
        self.c.executemany(
            """INSERT OR REPLACE INTO main.users_idcard(id, person_id, document, active, added, updated) VALUES(?, ?, ?, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)""",
            self.idCards,
        )

    def insert_groups(self):
        self.c.executemany(
            """INSERT OR REPLACE INTO main.users_group(id, name, added, updated) VALUES(?, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)""",
            self.groups,
        )

    def insert_group_timelimits(self):
        self.c.executemany(
            """INSERT OR REPLACE INTO main.users_grouplimit(id, weekday, from_time, to_time, group_id, added, updated) VALUES(?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)""",
            self.groupTimelimits,
        )

    def del_persons(self):
        if len(self.delPersons) > 0:
            self.c.execute(
                """DELETE FROM main.users_person WHERE main.users_person.id IN (?)""",
                str(self.delPersons),
            )

    def del_tags(self):
        if len(self.delTags) > 0:
            self.c.executemany(
                """DELETE FROM main.users_tag WHERE main.users_tag.id IN (?)""",
                self.delTags,
            )

    def del_groupTimelimits(self):
        if len(self.delGroupTimelimits) > 0:
            self.c.executemany(
                """DELETE FROM main.users_grouplimit WHERE main.users_grouplimit.id IN (?)""",
                self.delGroupTimelimits,
            )

    def add_person_to_group(self):
        self.c.executemany(
            """INSERT OR IGNORE INTO main.users_group_members(person_id, group_id) VALUES(?, ?)""",
            self.addPersonToGroup,
        )

    def del_person_from_group(self):
        self.c.executemany(
            """DELETE FROM main.users_group_members WHERE person_id = ? AND group_id = ?""",
            self.delPersonFromGroup,
        )

    def prepare_lists(self, tags):
        self.clear_insertion_lists()
        for tag in tags:
            if tag["uptd"] == 1:
                self.tags.append(
                    (
                        tag["id"],
                        tag["personId"],
                        tag["uid"],
                    )
                )
                if (tag["personId"],) not in self.persons:
                    self.persons.append((tag["personId"],))
                for groupId in tag["groupId"]:
                    self.addPersonToGroup.append((tag["personId"], groupId))
                    if (groupId,) not in self.groups:
                        self.groups.append((groupId,))
            else:
                if tag["uptd"] == -1:
                    self.delTags.append((tag["id"],))
                for groupId in tag["groupId"]:
                    if (
                        tag["personId"],
                        groupId,
                    ) not in self.delPersonFromGroup:
                        self.delPersonFromGroup.append(
                            (
                                tag["personId"],
                                groupId,
                            )
                        )

    def prepare_group_timelimits_list(self, data):
        self.clear_group_timelimits_insertion_lists()
        for i in data:
            if "delete" in i.keys():
                self.delGroupTimelimits.append((i["id"],))
            else:
                if i["fromTime"]:
                    i["fromTime"] += ":00"
                if i["toTime"]:
                    i["toTime"] += ":00"
                self.groupTimelimits.append(
                    (i["id"], i["weekday"], i["fromTime"], i["toTime"], i["groupId"])
                )

    def add_to_database(self, data, syncFromShift):
        if len(data["tags"]) == 0 and len(data["groupLimits"]) == 0:
            if syncFromShift != self.app.settings.EMPTY_LIST_SYNCROM_SHIFT_COUNT:
                newSyncFrom = self.calculateSyncFromShift()
                logging.info(
                    "Server responded empty list. Current syncFrom: {0} next syncFrom: {1}".format(
                        self.syncFrom, self.calculateSyncFromShift()
                    )
                )
                self.moveUpdateMarkerToPast(newSyncFrom)
                self.update_database(syncFromShift + 1)
                return False
            else:
                logging.error(
                    "Server responded empty list after {0} syncFrom shifts. Last syncFrom: {1}".format(
                        self.app.settings.EMPTY_LIST_SYNCROM_SHIFT_COUNT, self.syncFrom
                    )
                )
                return True

        self.prepare_lists(data["tags"])
        self.prepare_group_timelimits_list(data["groupLimits"])

        self.db = sqlite3.connect(
            self.app.settings.sqlite_file,
            detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,
        )
        self.c = self.db.cursor()

        # self.insert_idcard()
        if self.delTags:
            self.del_tags()
        if self.delPersonFromGroup:
            self.del_person_from_group()
        if self.delGroupTimelimits:
            self.del_groupTimelimits()

        if self.persons:
            self.insert_person()
        if self.tags:
            self.insert_tag()
        if self.groups:
            self.insert_groups()
        if self.addPersonToGroup:
            self.add_person_to_group()
        if self.groupTimelimits:
            self.insert_group_timelimits()

        self.db.commit()
        self.db.close()
        self.clearMarker()
        logging.info(
            "Update finished. Tags +{0} -{1}. GroupLimits +{2} -{3}".format(
                len(self.tags),
                len(self.delTags),
                len(self.groupTimelimits),
                len(self.delGroupTimelimits),
            )
        )
        return True

    def check_status(self):
        try:
            f = open(self.app.settings.UPDATE_MARKER_FILE, "r")
        except FileNotFoundError:
            f = open(self.app.settings.UPDATE_MARKER_FILE, "w")
            r = f.write("")
            f.close()
            return r

        r = f.read().strip()
        f.close()
        if r:
            self.syncFrom = int(r)
        return r

    def clearMarker(self):
        logging.debug("Clearing update marker")
        f = open(self.app.settings.UPDATE_MARKER_FILE, "w")
        r = f.write("")
        f.close()

    def moveUpdateMarkerToPast(self, newSyncFrom):
        f = open(self.app.settings.UPDATE_MARKER_FILE, "w")
        self.syncFrom = newSyncFrom
        r = f.write(str(self.syncFrom))
        f.close()

    def syncFinished(self):
        request = urllib.request.Request(
            url=self.app.settings.SYNC_URL
            + "sync/finished/tags/"
            + str(self.lastUpdate),
            headers={
                "Accept": "application/json, text/plain, */*",
                "X-Access-Token": self.app.settings.TOKEN,
                "Content-Type": "application/json; charset=utf-8",
            },
            method="GET",
        )

        try:
            with urllib.request.urlopen(request) as response_bytes:
                response = json.loads(response_bytes.read().decode("utf-8"))
                if response["result"] == "OK":
                    logging.info("Sync finished")

        except Exception as e:
            logging.debug("Error during sending sync finished request:" + str(e))
        finally:
            self.clearMarker()
