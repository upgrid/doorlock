import settings
from main import LockApplicationMain

class Suite():
    def __init__(self):
        '''Set up your environment for test package'''
        settings.ADD_UNKNOWN_IDCARD_TO_DATABASE = False
        settings.ADD_UNKNOWN_TAG_TO_DATABASE = False
        settings.sqlite_file = 'web/pytest.db.sqlite3'
        self.app = LockApplicationMain(settings, full=True)

    def getItem(self):
        return self.app.sync
