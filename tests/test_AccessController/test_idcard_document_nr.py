import pytest

from tests.test_AccessController import Suite

idcards = {
    'unknown': 'AA335662',
    'with_person': 'AA0825372',
    'without_person': 'A1628023',
    'unactive': 'AA0030027'
}

names = {
    'AA0825372': 'Valdur Kana',
    'A1628023': '',
}
access = Suite().getAccess()


def run_accept_id(item, excpect=None, checkname=None, readInfo=False):
    item = idcards[item]
    result, read_info = access.accept_id(item)

    name = access.lastPersonWithIdCard.name
    if checkname:
        assert name == names[item]
    else:
        assert result == excpect

    assert read_info == readInfo


@pytest.mark.parametrize(
    "item, excpect, readInfo",
    [
        ('unknown', False, True),
        ('with_person', True, False),
    ]
)
def test_access(item, excpect, readInfo):
    run_accept_id(item=item, excpect=excpect, readInfo=readInfo)


@pytest.mark.parametrize(
    "item",
    [
        ('without_person'),
        ('with_person'),
    ]
)
def test_name(item):
    run_accept_id(item=item, checkname=True)
