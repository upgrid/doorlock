import pytest

from tests.test_AccessController import Suite

access = Suite().getAccess()

@pytest.mark.parametrize("new_name", [
    'Kala'
])
def test_set_person_name(new_name):
    access.lastPersonWithIdCard.set_person_name(new_name)
    assert access.lastPersonWithIdCard.name == new_name
