import pytest

from tests.test_AccessController import Suite

tags = {
    'unknown': '44 44 55 66',
    'with_person': '04 E7 ED A7',
    'without_person': 'A5 A6 27 00',
    'without_person_short': 'A5 A6 27',
    'with_person_short': 'E7 ED A7',
    'unknown_short': '33 55 77',
    'unactive': '04 32 C7 7A 97 3C 80'

}

names = {
    '04 E7 ED A7': 'Valdur Kana',
    'E7 ED A7': 'Valdur Kana',
    'A5 A6 27 00': '',
    'A5 A6 27': ''
}
access = Suite().getAccess()


def run_accept_nfc(item, excpect=None, normal=None, checkname=None):
    item = tags[item]
    if normal is None:
        result = access.accept_nfc(tag=item)
    else:
        result = access.accept_nfc(tag=item, normal=normal)

    name = access.lastPersonWithTag.name

    if checkname:
        assert name == names[item]
    else:
        assert result == excpect


@pytest.mark.parametrize(
    "item, excpect",
    [
        ('unknown', False),
        ('unactive', False),
        ('with_person', True),
        ('with_person_short', True),
        ('unknown_short', False),
        ('without_person', True),
        ('without_person_short', True)
    ]
)
def test_access(item, excpect):
    run_accept_nfc(item=item, excpect=excpect)


@pytest.mark.parametrize(
    "item", [
        'without_person',
        'with_person',
        'with_person_short',
        'without_person_short'
    ]
)
def test_name(item):
    run_accept_nfc(item=item, checkname=True)
