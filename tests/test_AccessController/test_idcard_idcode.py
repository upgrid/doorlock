import pytest

from tests.test_AccessController import Suite

persons = {
    'active': {
        'idcode': 90000002352,
        'name': 'Kati Karu',
        'documentcode': 'XA0010001'
    },

    'unactive': {
        'idcode': 22222222,
        'name': 'Unactive Person',
        'documentcode': ''
    }
}

access = Suite().getAccess()



def run_accept_person(item, excpect=None, checkname=None):
    item = persons[item]

    result, read_info = access.accept_person(item['idcode'])

    if not checkname:
       access.lastPersonWithIdCard.name = item['name']

    if checkname:
        assert access.lastPersonWithIdCard.name == item['name']
    else:
        assert result == excpect


@pytest.mark.parametrize(
    "item, excpect",
    [
        ('active', True),
        ('unactive', False)
    ])
def test_accept(item, excpect):
    run_accept_person(item, excpect)

@pytest.mark.parametrize(
    "item",
    [
        ('active'),
        ('unactive')
    ])
def test_name(item):
    run_accept_person(item, checkname=True)

