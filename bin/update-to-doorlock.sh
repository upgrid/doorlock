#!/usr/bin/env bash
sudo service doorlock stop;
sudo service doorlock-web stop;
cd ..
mv proof-on-concept doorlock;
ln -s doorlock proof-on-concept;
find doorlock -type f -exec sed -i 's/grid-dev\/proof-on-concept/upgrid\/doorlock/g' {} +;
sudo sed -i 's/proof-on-concept/doorlock/g' /etc/systemd/system/doorlock*;
sudo systemctl daemon-reload;
sudo service doorlock start;
sudo service doorlock-web start;