#!/usr/bin/env bash

LOCK_PIN=21

create_open_close  () {

cat > close.sh << EOF
#!/bin/bash

#Lock door
pinctrl set $LOCK_PIN op pn dl

EOF

cat > open.sh << EOF
#!/bin/bash

#Unlock door
pinctrl set $LOCK_PIN op pn dh

EOF

chmod +x open.sh
chmod +x close.sh
}

presistent_journal() {
    sudo sed -i 's/\#Storage=auto/Storage=persistent/g' /etc/systemd/journald.conf
}

weekly_restart() {
    sudo tee /etc/con.weekly/doorlock-restart > /dev/null << EOF
#!/bin/bash
sudo service doorlock restart

EOF
chmod +x /etc/con.weekly/doorlock-restart
}

echo "Do you wan't restart doorlock service weekly?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) weekly_restart ; break;;
        No ) break;;
    esac
done

echo "Do you wan't make journal presistent?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) presistent_journal ; break;;
        No ) break;;
    esac
done

# Create open.sh and close.sh , add it into crontab
echo "Do you wan't create open.sh and close.sh?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) create_open_close ; break;;
        No ) break;;
    esac
done


#!/usr/bin/env bash

install_system_deps () {
    sudo apt update

    # Install deps from package manager
    sudo apt install -y pcscd opensc python3-pip sqlite3 pcsc-tools swig libpcsclite-dev python3-setuptools

    # GPIO module install only works with raspbian
    sudo apt install -y python3-pigpio

    pip3 install --upgrade --break-system-packages pyscard Django argparse watchdog pytest
}

setup_launchers() {
    mkdir bin

    cat > bin/doorlock.sh << EOF
#!/bin/sh

cd $PWD
python3 main.py

EOF

    cat > bin/doorlock-web.sh << EOF
#!/bin/sh

cd $PWD/web
python3 manage.py runserver 0.0.0.0:8000

EOF

    cat > debug.sh << EOF
#!/bin/bash

#Kill previous controller program
sudo service doorlock stop

#Open door controller fast locking, verbose, debug mode
cd $PWD
python3 main.py -d -v -f

EOF




    sudo tee /etc/systemd/system/doorlock.service > /dev/null << EOF
[Unit]
Description=Doorlock service
Requires=pcscd.service pigpiod.service
After=pcscd.service pigpiod.service

[Service]
ExecStart=$PWD/bin/doorlock.sh
Type=idle
User=$USER

[Install]
WantedBy=multi-user.target

EOF



    sudo tee /etc/systemd/system/doorlock-web.service > /dev/null << EOF
[Unit]
Description=Doorlock web admin

[Service]
ExecStart=$PWD/bin/doorlock-web.sh
User=$USER

[Install]
WantedBy=multi-user.target

EOF

    chmod +x bin/doorlock.sh
    chmod +x bin/doorlock-web.sh
    chmod +x debug.sh

    sudo gpasswd -a $USER systemd-journal

    # Wiegand reading depends on pigpiod
    sudo systemctl enable pigpiod.service
}


backup_database (){
    if [ -f web/db.sqlite3 ]; then
        mv web/db.sqlite3 web/backups/db_$(date +%s).sqlite3
    fi

}

install_acr_black_reader (){
    wget http://www.acs.com.hk/download-driver-unified/5128/ACS-Unified-PKG-Lnx-112-P.zip
    unzip ACS-Unified-PKG-Lnx-112-P.zip
     sudo dpkg -i ACS-Unified-PKG-Lnx-113-P/acsccid_linux_bin-1.1.3/raspbian/jessie/libacsccid1_1.1.3-1~bpo8+1_armhf.deb
    rm -rf acsccid_linux_bin-1.1.2 ACS-Unified-PKG-Lnx-112-P.zip ACS-Unified-PKG-Lnx-113-P

}


echo "Do you wan't install app dependencies?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) install_system_deps ; break;;
        No ) break;;
    esac
done

echo "Do you wan't install ACR122U (Black reader) drivers (works in raspbian)?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) install_acr_black_reader ; break;;
        No ) break;;
    esac
done

echo "Do you wan't create launchers for systemd autostart, debug?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) setup_launchers ; break;;
        No ) break;;
    esac
done
echo "Do you wan't edit timezone?"
echo "Select Europe and then Tallin . (use e, t for faster selection)"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo dpkg-reconfigure tzdata; break;;
        No ) break;;
    esac
done


echo "Do you wan't to autostart doorlock.service?"
select yn in "Yes" "No" "Nothing"; do
    case $yn in
        Yes ) sudo systemctl enable doorlock.service; break;;
        No ) sudo systemctl disable doorlock.service; break;;
        Nothing) break;;
    esac
done

echo "Do you wan't to autostart doorlock-web.service?"
select yn in "Yes" "No" "Nothing"; do
    case $yn in
        Yes ) sudo systemctl enable doorlock-web.service; break;;
        No ) sudo systemctl disable doorlock-web.service; break;;
        Nothing) break;;
    esac
done

echo "Do you wan't use test database?"
select yn in "Yes" "Clean" "Nothing"; do
    case $yn in
        Yes ) backup_database ; sqlite3 web/db.sqlite3 ".read web/dumps/dump.sql"; python3 web/manage.py migrate ; break;;
        Clean ) backup_database ; sqlite3 web/db.sqlite3 ".read web/dumps/clean.sql" ; python3 web/manage.py migrate; break;;
        Nothing) break;;
    esac
done

echo "Do you wan't runn app in debug mode?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) ./debug.sh ; break;;
        No ) break;;
    esac
done