import logging
import threading

from smartcard.CardMonitoring import CardObserver
from smartcard.Exceptions import CardConnectionException
from smartcard.System import readers
from smartcard.util import toHexString, toBytes
from smartcard.pcsc.PCSCPart10 import (
    getFeatureRequest,
    hasFeature,
    getTlvProperties,
    FEATURE_CCID_ESC_COMMAND,
    SCARD_SHARE_DIRECT,
)
from doorActionTypes import DoorActionSrc

# Card Listener
class CardController(CardObserver):
    def __init__(self, app):
        """
        :type app: LockApplicationMain
        """
        self.app = app
        super().__init__()
        self.ID_atrs = [
            "3B FE 18 00 00 80 31 FE 45 45 73 74 45 49 44 20 76 65 72 20 31 2E 30 A8",
            "3B 6E 00 00 45 73 74 45 49 44 20 76 65 72 20 31 2E 30",
            "3B 6E 00 FF 45 73 74 45 49 44 20 76 65 72 20 31 2E 30",
            "3B DE 18 FF C0 80 B1 FE 45 1F 03 45 73 74 45 49 44 20 76 65 72 20 31 2E 30 2B",
            "3B FE 94 00 FF 80 B1 FA 45 1F 03 45 73 74 45 49 44 20 76 65 72 20 31 2E 30 43",
            "3B FA 18 00 00 80 31 FE 45 FE 65 49 44 20 2F 20 50 4B 49 03",
            "3B FE 18 00 00 80 31 FE 45 80 31 80 66 40 90 A4 16 2A 00 83 0F 90 00 EF",
            "3B FE 18 00 00 80 31 FE 45 80 31 80 66 40 90 A4 16 2A 00 83 01 90 00 E1",
        ]
        self.IDEMIA_atrs = [
            "3B DB 96 00 80 B1 FE 45 1F 83 00 12 23 3F 53 65 49 44 0F 90 00 F1"
        ]
        # More info about Estonian ID Card: http://www.id.ee/public/TB-SPEC-EstEID-Chip-App-v3.4.pdf

        # id_card Application Protocol Data Unit (apdu)
        # look at code in commit 4728181 to see how name and personal identification code was read (with list instead
        # of dictionary)
        self.id_card_data_unit = {
            "select master file": "00A4000C",
            "select catalogue EEEE": "00A4010C02EEEE",
            "select file 5044": "00A4020402504400",
            "read personal identification code": "00B2070400",
            "read first name line 1": "00B2020400",
            "read first name line 2": "00B2030400",
            "read surname": "00B2010400",
            "read document number": "00B2080400",
        }

        self.IDEMIA_card_data_unit = {
            "select master file": "00A4000C",
            "select Personal data DF": "00A4010C025000",
            "read personal identification code": "00A4020C025006",
            "read first name": "00A4020C025002",
            "read surname": "00A4020C025001",
            "read document number": "00A4020C025007",
            "read binary": "00B0000000",
        }
        self.nfc_data_unit = "FFCA000000"  # nfc Application Protocol Data Unit (apdu)
        self.actionSrc = DoorActionSrc.CARD

    def update(self, observable, cards):
        try:
            (added_cards, removed_cards) = cards
            for card in added_cards:
                logging.debug("Card added: {0}".format(card))
                if self.toHexString(card.atr) in self.ID_atrs:
                    if self.handle_id_card(card):
                        if self.app.settings.FALLBACK_TO_IDCODE_CHECK:
                            self.app.door.unlock(
                                self.app.access.lastPersonWithIdCard.idCard.cardNr,
                                name=self.app.access.lastPersonWithIdCard.name,
                                idcode=self.app.access.lastPersonWithIdCard.idCard.idCode,
                                src=self.actionSrc,
                            )
                        else:
                            self.app.door.unlock(
                                self.app.access.lastPersonWithIdCard.idCard.cardNr,
                                name=self.app.access.lastPersonWithIdCard.name,
                                src=self.actionSrc,
                            )
                elif self.toHexString(card.atr) in self.IDEMIA_atrs:
                    if self.handle_IDEMIA_card(card):
                        if self.app.settings.FALLBACK_TO_IDCODE_CHECK:
                            self.app.door.unlock(
                                self.app.access.lastPersonWithIdCard.idCard.cardNr,
                                name=self.app.access.lastPersonWithIdCard.name,
                                idcode=self.app.access.lastPersonWithIdCard.idCard.idCode,
                                src=self.actionSrc,
                            )
                        else:
                            self.app.door.unlock(
                                self.app.access.lastPersonWithIdCard.idCard.cardNr,
                                name=self.app.access.lastPersonWithIdCard.name,
                                src=self.actionSrc,
                            )
                elif (
                    "Contactless" in card.reader
                    or "ACS" in card.reader
                    and "SAM" not in card.reader
                ):
                    if self.handle_nfc(card):
                        self.app.door.unlock(
                            self.app.access.lastPersonWithTag.tag.uid,
                            name=self.app.access.lastPersonWithTag.name,
                            src=self.actionSrc,
                        )

            for card in removed_cards:
                logging.debug("Card removed")
                if (
                    self.app.access.lastPersonWithIdCard.idCard.cardNr
                    and "Contactless" not in card.reader
                    and "ACS" not in card.reader
                ):
                    self.app.access.clear_all()
                    continue
                self.app.access.clear_tag()
                continue
        except Exception as e:
            logging.exception(e)

    def card_action(self, data_unit, card, decode=False):
        (response, sw1, sw2) = card.connection.transmit(toBytes(data_unit))
        if sw1 == 97:  # Card sends selected bytestring lenght
            (response, sw1, sw2) = card.connection.transmit(
                toBytes("00C00000" + self.toHexString([sw2]))
            )
        if sw1 != 144:  # Not OK
            raise Exception(
                "Response status is: %x %x used adpu: %s" % (sw1, sw2, data_unit)
            )

        if decode:
            if response[0] in [0, 32]:  # Check if response is empty
                return ""
            return (
                bytearray(response)
                .decode(encoding="ISO 8859-1", errors="strict")
                .strip()
            )
        else:
            return response

    def handle_id_card(self, card):
        try:
            card.connection = card.createConnection()
            card.connection.connect()

            for part in (
                "select master file",
                "select catalogue EEEE",
                "select file 5044",
            ):
                self.card_action(self.id_card_data_unit[part], card)

            if self.app.settings.FALLBACK_TO_IDCODE_CHECK:
                idcode = self.card_action(
                    self.id_card_data_unit["read personal identification code"],
                    card,
                    True,
                )
                accept, read_data = self.app.access.accept_person(idcode)
            else:
                document_nr = self.card_action(
                    self.id_card_data_unit["read document number"], card, True
                )
                accept, read_data = self.app.access.accept_id(document_nr)

            if read_data:
                self.read_person_info(card)

            return accept

        except CardConnectionException:
            logging.error("Connecting to card failed")
        except Exception as e:
            logging.error(e)

        return None

    def handle_IDEMIA_card(self, card):
        try:
            card.connection = card.createConnection()
            card.connection.connect()

            for part in ("select master file", "select Personal data DF"):
                self.IDEMIA_card_action(part, card)

            if self.app.settings.FALLBACK_TO_IDCODE_CHECK:
                idcode = self.IDEMIA_card_action(
                    "read personal identification code", card, True, True
                )
                accept, read_data = self.app.access.accept_person(idcode)
            else:
                document_nr = self.IDEMIA_card_action(
                    "read document number", card, True, True
                )
                accept, read_data = self.app.access.accept_id(document_nr)

            if read_data:
                self.read_person_info(card)

            return accept

        except CardConnectionException as e:
            logging.error("Connecting to card failed: " + str(e))
        except Exception as e:
            logging.exception(e)

        return None

    def IDEMIA_card_action(self, data_unit, card, decode=False, read_byte=False):
        try:
            (response, sw1, sw2) = card.connection.transmit(
                toBytes(self.IDEMIA_card_data_unit[data_unit])
            )
            if read_byte:
                (response, sw1, sw2) = card.connection.transmit(toBytes("00B00000"))
            if str(sw1).upper() not in ["90", "6E"]:  # Not OK
                error_str = ""
                if "%x%x".upper() % (sw1, sw2) == "6A86":
                    error_str = "Incorrect parameters P1-P2"
                raise Exception(
                    "Response status is: %x %x (%s) used adpu: '%s'"
                    % (sw1, sw2, error_str, data_unit)
                )

            if decode:
                if response[0] in [0, 32]:  # Check if response is empty
                    return ""
                return (
                    bytearray(response)
                    .decode(encoding="UTF-8", errors="strict")
                    .strip()
                )
            else:
                return response
        except Exception as e:
            raise Exception(
                "Exception during card action: '{0}' ".format(data_unit) + str(e)
            )

    def handle_nfc(self, card):
        try:
            card.connection = card.createConnection()
            card.connection.connect()
            nfc_uid = self.toHexString(self.card_action(self.nfc_data_unit, card))
            self.buzz_r502(card)
            return self.app.access.accept_nfc(nfc_uid, normal=True)

        except CardConnectionException as e:
            logging.error("Connecting to tag failed: " + str(e))
        except Exception as e:
            logging.error(e)

        return None

    def read_person_info(self, card):
        name = ""
        for part in [
            "read first name line 1",
            "read first name line 2",
            "read surname",
        ]:
            resp = self.card_action(self.id_card_data_unit[part], card, True)
            if resp != "":
                if name:
                    name += " " + resp
                else:
                    name = resp

        self.app.access.lastPersonWithIdCard.name = name.title()

        if not self.app.access.lastPersonWithIdCard.idcode:
            self.app.access.lastPersonWithIdCard.idcode = self.card_action(
                self.id_card_data_unit["read personal identification code"], card, True
            )

        if not self.app.access.lastPersonWithIdCard.cardNr:
            self.app.access.lastPersonWithIdCard.cardNr = self.card_action(
                self.id_card_data_unit["read document number"], card, True
            )

        logging.info(
            "Person info: {0} {1} {2}".format(
                self.app.access.lastPersonWithIdCard.name,
                self.app.access.lastPersonWithIdCard.idCard.idCode,
                self.app.access.lastPersonWithIdCard.IdCard.cardNr,
            )
        )

    def buzz_r502(self, card):
        t = threading.Thread(
            target=self._send_command_buzz_r502, args=(), kwargs={"card": card}, daemon=True
        )
        t.start()

    def buzz_start_r502(self, reader):
        try:
            if "Feitian R502 [R502 Contactless Reader]" not in reader:
                exit()
            reader = list(filter(lambda r: r.name == reader, readers()))[0]
            card_connection = reader.createConnection()
            card_connection.connect(mode=SCARD_SHARE_DIRECT)

            # get CCID Escape control code
            feature_list = getFeatureRequest(card_connection)

            ccid_esc_command = hasFeature(feature_list, FEATURE_CCID_ESC_COMMAND)
            if ccid_esc_command is None:
                raise Exception("The reader does not support FEATURE_CCID_ESC_COMMAND")

            # get the TLV PROPERTIES
            tlv = getTlvProperties(card_connection)
            if tlv["PCSCv2_PART10_PROPERTY_wIdVendor"] == 2414 and (
                tlv["PCSCv2_PART10_PROPERTY_wIdProduct"] == 1549
            ):
                result = card_connection.control(
                    ccid_esc_command, toBytes("A55A600000")
                )
                if result:
                    print(result)
        except Exception as e:
            logging.error("R502 reader beeping failed: " + str(e))

    def buzz_end_r502(self, card):
        try:
            if "Feitian R502 [R502 Contactless Reader]" not in card.reader:
                pass
            card_connection = card.createConnection()
            card_connection.connect(mode=SCARD_SHARE_DIRECT)

            # get CCID Escape control code
            feature_list = getFeatureRequest(card_connection)

            ccid_esc_command = hasFeature(feature_list, FEATURE_CCID_ESC_COMMAND)
            if ccid_esc_command is None:
                raise Exception("The reader does not support FEATURE_CCID_ESC_COMMAND")

            # get the TLV PROPERTIES
            tlv = getTlvProperties(card_connection)
            if tlv["PCSCv2_PART10_PROPERTY_wIdVendor"] == 2414 and (
                tlv["PCSCv2_PART10_PROPERTY_wIdProduct"] == 1549
            ):
                result = card_connection.control(
                    ccid_esc_command, toBytes("A55A600100")
                )
                if result:
                    print(result)
        except Exception as e:
            logging.error("R502 reader beeping failed: " + str(e))

    def _send_command_buzz_r502(*arg, **kwargs):
        card = kwargs["card"]
        try:
            if "Feitian R502" not in card.reader:
                pass
            card_connection = card.createConnection()
            card_connection.connect(mode=SCARD_SHARE_DIRECT)

            # get CCID Escape control code
            # feature_list = getFeatureRequest(card_connection)
            # ccid_esc_command = hasFeature(feature_list, FEATURE_CCID_ESC_COMMAND)
            ccid_esc_command = 1107296257

            result = card_connection.control(
                ccid_esc_command, toBytes("A55A6002020064")
            )
            if result:
                print(result)
        except Exception as e:
            logging.error("R502 reader beeping failed: " + str(e))

    def toHexString(self, item):
        if item[-1] == 0:
            return self.toHexString(item[:-1])
        else:
            return toHexString(item
)