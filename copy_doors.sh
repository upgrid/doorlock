#!/usr/bin/env bash

hosts="3k-tsolutions-1 3k-tsolutions-2 3k-tsolutions-3"
answer=0
echo "Do you wan't to add your ssh key to pi auth keys?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) answer=1 ; break;;
        No ) break;;
    esac
done

for host in ${hosts}; do
    echo "Working with host: " ${host};
    if [ ${answer} -eq 1 ]; then
        ssh-copy-id pi@${host}
    fi
    scp web/db.sqlite3 pi@${host}:/home/pi/proof-on-concept/web/
done