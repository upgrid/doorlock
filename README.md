Live demo: 

* http://vs15.dynu.net:8000/doorlog/
* http://vs15.dynu.net:8000/users/tags/
* http://vs15.dynu.net:8000/admin/ ( admin : admin )




rpi-eidlock
===========

Electroni eID card lock for Raspberry Pi @ hackest.org
Based on https://github.com/martinpaljak/rpi-eidlock/blob/master/test.py  ( buggy code, but was nice start)

# Features : #
 
* Read name and ID-code from card
* Opens door when ID is on database
* Show log at web

# Portal #
User: admin
Pass: admin

# Test card reader #

```
sudo apt-get install pcsc-tools
pcsc_scan
```

# Installing: #

```
#!/bin/bash

sudo apt-get install git
ssh-keygen 
cat ~/.ssh/id_rsa.pub
read -p "Click on your bitbucket profile pic > Manage profile > SSH Keys > Add key.  Click enter when finished "
git clone git@bitbucket.org:upgrid/doorlock.git
cd doorlock
./setup.sh

```

For button and lock,  connect GPIO pin and GND

# Closed ports #
Use https://ngrok.com. 
Need free registration for non http data.


# For updating to new url please  run this script #

```
#!/bin/bash
sudo service doorlock stop;
sudo service doorlock-web stop;
mv proof-on-concept doorlock;
ln -s doorlock proof-on-concept;
find doorlock -type f -exec sed -i 's/proof-on-concept/doorlock/g' {} +;
find doorlock -type f -exec sed -i 's/grid-dev/upgrid/g' {} +;
sudo sed -i 's/proof-on-concept/doorlock/g' /etc/systemd/system/doorlock*;
sudo systemctl daemon-reload;
sudo service doorlock start;
sudo service doorlock-web start;
```