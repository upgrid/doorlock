#!/usr/bin/python
import argparse
import logging
import os
import signal
import sys
import time

import threading

sys.path.append(os.path.join(os.path.dirname(__file__), "."))

from doorActionTypes import DoorActionSrc
from access_controller import AccessController
from sync import Sync
from door import Door
from lock_automation import Automatic
from sendLogRetry import SendLogRetry

try:
    import pigpio
except Exception:
    pigpio = False


class LockApplicationMain:
    remoteOpenEndTime = None
    logSendRetryInteval = 15 * 60
    logSendDatabaseCheckInterval = 10
    logLevel = logging.INFO
    verbose = False
    lastLog = []


    def ensure_gpio_ready(self):
        while True:
            try:
                with open('/dev/gpiomem') as x:
                    break
            except (FileNotFoundError, PermissionError):
                pass
            time.sleep(1)

    def areAllSamePerson(self):
        return len(set(self.lastLog)) == 1 and len(self.lastLog) == 3

    def addLastLog(self, person_id):
        temp = self.lastLog[(0 - 3 + 1):]
        temp.append(person_id)
        self.lastLog = temp



    def setup(self):

        # initialize pins
        try:
            import RPi.GPIO as GPIO

            self.GPIO = GPIO
        except Exception:
            self.GPIO = False

        if self.GPIO:
            self.ensure_gpio_ready()
            self.GPIO.setmode(self.GPIO.BCM)
            self.GPIO.setup(
                self.settings.BUTTON_PIN, self.GPIO.IN, pull_up_down=self.GPIO.PUD_UP
            )
            if self.settings.TWO_WIEGAND_READERS:
                self.GPIO.setup(
                    self.settings.SECOND_WIEGAND_TRIGGER_PIN,
                    self.GPIO.IN,
                    pull_up_down=self.GPIO.PUD_UP,
                )

    def wiegand_callback(self, bits, value):
        if bits < 26:
            return

        if self.access.accept_nfc(value):
            if self.areAllSamePerson() and False:
                self.door.unlock(
                    "1h open " + value,
                    name=self.access.lastPersonWithTag.name,
                    src=DoorActionSrc.CARD,
                    )
            else:
                self.door.unlock(
                    "Wiegand tag " + value,
                    name=self.access.lastPersonWithTag.name,
                    src=DoorActionSrc.CARD,
                )
                self.access.clear_tag()

    def cleanup(self):
        if self.settings.ALLOW_REMOTE_OPEN_WITH_TIME:
            self.openByFile_observer.stop()
            self.openByFileEnd_observer.stop()
            self.openByFile_observer.join()
            self.openByFileEnd_observer.join()
        if self.settings.ALLOW_DATABASE_SYNC:
            self.databaseUpdater_observer.stop()
            self.databaseUpdater_observer.join()
        if "PYSCARD" in self.settings.READER_TYPES:
            self.cardmonitor.deleteObserver(self.cardobserver)
        if self.GPIO:
            self.GPIO.cleanup()

    def __init__(self, settings, full=True):
        logging.info(">> App init ")
        self.settings = settings
        self.setup()
        self.SYNC_URLhostName = self.settings.SYNC_URL.split("/")[2].split(":")[0]
        self.sendLive_event = threading.Event()
        self.sync = Sync(self)
        self.access = AccessController(self)
        self.door = Door(self)
        self.automation = Automatic(self)
        self.sendLogRetry = SendLogRetry(self)


        if full:
            from watchdog.observers import Observer as WatchdogObserver
            from open_door_by_file import OpenByFile
            from open_door_by_file_end import OpenByFileEnd
            from button import Button
            from database_updater import DatabaseUpdater

            self.database_updater = DatabaseUpdater(self)

            self.button = Button(self)
            self.openByFile = OpenByFile(self)
            self.openByFileEnd = OpenByFileEnd(self)
            self.openByFileEnd_observer = WatchdogObserver()
            self.databaseUpdater_observer = WatchdogObserver()
            self.openByFile_observer = WatchdogObserver()

            if "PYSCARD" in self.settings.READER_TYPES:
                from card_controller import CardController
                from smartcard.CardMonitoring import CardMonitor

                self.cardobserver = CardController(self)
                self.cardmonitor = CardMonitor()

            if self.settings.ALLOW_STATUS_SYNC:
                self.sendLive_event.set()

    def run(self):
        logging.info(">> Hello World!")

        if self.settings.ALLOW_STATUS_SYNC:
            logSyncThread = threading.Thread(target=self.sync.run, name="logSyncThread")
            logSyncThread.daemon = True
            logSyncThread.start()

        self.openByFileEnd_observer.schedule(
            self.openByFileEnd,
            path="./" + self.settings.REMOTE_OPEN_FILE_END,
                                             recursive=False,
        )

        self.openByFile_observer.schedule(
            self.openByFile, path="./" + settings.REMOTE_OPEN_FILE, recursive=False
        )

        self.databaseUpdater_observer.schedule(self.database_updater, path=self.settings.UPDATE_MARKER_FILE,
                                               recursive=False,
        )

        automationThread = threading.Thread(target=self.automation.run, name='Automation')
        automationThread.daemon = True

        if self.settings.ALLOW_REMOTE_OPEN_WITH_TIME:
            self.openByFile_observer.start()
            self.openByFileEnd_observer.start()
            self.openByFile.handleRemoteOpen(init=True)
            if self.remoteOpenEndTime:
                self.door.firstUnlock(
                    "Init: Resume from remote open until by file",
                    autolock=False,
                    force=True,
                )
                self.automation.setNewLastOpen()
            else:
                if not self.automation.check_if_worktime():
                    self.door.firstLock("Init remote open until by file")
                    self.automation.setNewLastLock()
            self.openByFileEnd.handleRemoteOpenEnd(init=True)

        if (
            not self.settings.USE_AUTOMATIC_OPENING
            and not self.settings.ALLOW_REMOTE_OPEN_WITH_TIME
        ):
            self.door.firstLock("Init", force=True)
        else:
            automationThread.start()

        if self.settings.ALLOW_DATABASE_SYNC:
            self.database_updater.handleStateMotification()
            self.databaseUpdater_observer.start()

        if pigpio:
            if "WIEGAND" in self.settings.READER_TYPES:
                import wiegand

                w = wiegand.Decoder(self.wiegand_callback)
        else:
            self.door.unlock(
                "Test unlock (only at computer)",
                force=True,
                autolock=False,
                src=DoorActionSrc.CARD,
            )

        if "PYSCARD" in self.settings.READER_TYPES:
            self.cardmonitor.addObserver(self.cardobserver)

        if self.settings.ALLOW_BUTTON:
            self.button.run()
        else:
            self.button.run_emulation()

if __name__ == "__main__":
    import settings

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--debug",
        help="Print lots of debugging statements",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=logging.INFO,
    )

    parser.add_argument(
        "-v",
        "--verbose",
        help="Show messages on screen",
        action="store_const",
        dest="verbose",
        const=True,
        default=False,
    )

    parser.add_argument(
        "-f",
        "--fast",
        help="Faster door locking",
        action="store_const",
        dest="timeout",
        const=1,
        default=settings.LOCK_OPEN_TIME,
    )

    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(
            level=args.loglevel, format="%(asctime)s | %(levelname)s | %(message)s"
        )

    else:
        logging.basicConfig(
            filename="uks.log",
            level=args.loglevel,
            format="%(asctime)s | %(levelname)s | %(message)s",
        )

    # Handle ctrl-c
    def sigint_handler(signum, frame):
        logging.info("<< Closing")
        app.cleanup()
        exit()

        # CTRL-C signal

    signal.signal(signal.SIGINT, sigint_handler)
    signal.signal(signal.SIGTERM, sigint_handler)

    app = LockApplicationMain(settings)
    app.door.opentime = args.timeout
    app.logLevel = args.loglevel
    app.verbose = args.verbose

    if args.loglevel == logging.DEBUG:
        app.logSendRetryInteval = 5
        app.logSendDatabaseCheckInterval = 5
    app.run()