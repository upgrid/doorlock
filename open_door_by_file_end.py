from time import time

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from door import Door
import json
import settings
import logging
from datetime import datetime


class OpenByFileEnd(FileSystemEventHandler):
    def __init__(self, app):
        """
        :type app: LockApplicationMain
        """
        self.app = app
        super().__init__()

    def on_modified(self, event):
        if event.src_path == "./" + settings.REMOTE_OPEN_FILE_END:
            self.handleRemoteOpenEnd()

    def handleRemoteOpenEnd(self, init=False):
        try:
            f = open(settings.REMOTE_OPEN_FILE_END, "r")
            r = f.read().strip()
            f.close()
            if r:
                if self.app.sync.sendRemoteOpenEnd():
                    self.clearRemoteOpenFileEnd()
        except Exception as e:
            logging.exception(e)

    def clearRemoteOpenFileEnd(self):
        f = open(settings.REMOTE_OPEN_FILE_END, "w")
        f.write("")
        f.close()

    def saveRemoteOpenFileEnd(self):
        f = open(settings.REMOTE_OPEN_FILE_END, "w")
        f.write("True")
        f.close()
