#!/usr/bin/env bash

for f in *.log; do
    while read -r line
    do
        echo $(grep -a -m 1 "$line" $f) >> "$f"_tags
    done < tsub
done