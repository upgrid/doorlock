import json

import requests

log = []
log.append({
    "doorId": 7,
    "uid": "04 E7 ED A7",
    "eventDate": '2018-10-06 16:20:59.581',
    "open": 1
})

hostname = "http://localhost:9090/"
token="x59w4s7rjvorj6diaoe49g43p0b1ygwc"

url = hostname + "api/doors/importlog"



str_payload = json.dumps(log)

headers = {
    'accept': "application/json, text/plain, */*",
    'origin': hostname,
    'x-access-token': token,
    'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
    'content-type': "application/json",
    'dnt': "1",
    'referer': hostname,
    'accept-encoding': "gzip, deflate, br",
    'accept-language': "et-EE,et;q=0.9,en-US;q=0.8,en;q=0.7",
    'cache-control': "no-cache",
}

response = requests.request("POST", url, data=str_payload, headers=headers)

print(response.text)

