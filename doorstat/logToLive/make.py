import csv
import json
import numpy as np
import requests
from doorActionTypes import DoorActionSrc
from doorActionTypes import DoorActionTypes
csvfile = csv.DictReader(open('doors.csv', 'r'))

doors = {}

for line in csvfile:
    doors[line["door"] + "_uks.log"] = line["id"]

log = []
data = []


# if (row.includes("Unknown tag:")) {
# action.uid = row[2].slice(48)
# }
# else if (raw.includes("Wiegand tag")) {
# action.open = +true;
# action.uid = row[2].slice(61)

def collect_uids(name, doorId):
    print("Working with doorname {0} ; doorid: {1}".format(name, doorId))
    f = open(name)
    for line in f:
        if "2022-09-" not in line:
            continue
        l = line.strip().split(" | ")
        if len(l) >= 3:
            t = l[-1].split(": ")[-1]
            if "Wiegand tag" in line and 'of active time. Reason: Wiegand tag' not in line:
                eventType = DoorActionTypes.UNLOCK
                eventOpen = 1
                tag = line[61:].replace("\n", "").strip()
                log.append({
                    "doorId": doorId,
                    "uid": tag,
                    "eventDate": l[0].replace(",", "."),
                    "open": eventOpen,
                    "type": str(eventType.value),
                    'src': str(DoorActionSrc.CARD.value)
                })
                data.append([l[0], doorId, tag, str(eventType.value)])
            elif "Unknown tag" in line:
                eventType = DoorActionTypes.UNKNOWN_CARD
                eventOpen = 0
                tag = line[48:].replace("\n", "").strip()
                log.append({
                    "doorId": doorId,
                    "uid": tag,
                    "eventDate": l[0].replace(",", "."),
                    "open": eventOpen,
                    "type": str(eventType.value),
                    'src': str(DoorActionSrc.CARD.value)
                })
                data.append([l[0], doorId, tag, str(eventType.value)])
            elif "Unlocking door" in line:
                eventType = DoorActionTypes.UNLOCK
                eventOpen = 1
                tag = line[49:].replace("\n", "").strip()
                log.append({
                    "doorId": doorId,
                    "uid": tag,
                    "eventDate": l[0].replace(",", "."),
                    "open": eventOpen,
                    "type": str(eventType.value),
                    'src': str(DoorActionSrc.CARD.value)
                })
                data.append([l[0], doorId, tag, str(eventType.value)])
    f.close()


files = doors.keys()


for k in files:
    try:
        collect_uids("/home/valdur/arendus/upgrid/doorlock/doorstat/logToLive/cut/" + k, doors[k])
    except FileNotFoundError:
        print("No file for door: " + k)

with open('lock.csv', 'w') as csvfile:
    spamwriter = csv.writer(csvfile)
    spamwriter.writerow(["eventDate", "doorId", "uid", "eventType"])
    spamwriter.writerows(data)


hostname = "http://localhost:9090/"
token = "xi99wgzu5sp7g8c3exawjnqcnmoi7g9a"

url = hostname + "api/doors/importlog"

i = 0
f = open('response.log', 'a')
for part in np.array_split(log, 30):
    str_payload = json.dumps(list(part))
    i += 1

    headers = {
        'accept': "application/json, text/plain, */*",
        'origin': hostname,
        'x-access-token': token,
        'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
        'content-type': "application/json",
        'dnt': "1",
        'referer': hostname,
        'accept-encoding': "gzip, deflate, br",
        'accept-language': "et-EE,et;q=0.9,en-US;q=0.8,en;q=0.7",
        'cache-control': "no-cache",
    }

    response = requests.request("POST", url, data=str_payload, headers=headers)
    print(i)
    print(response.text)
    f.write("" + str(i) + " : " + str(response.text))

# print(response.text)
