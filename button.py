from datetime import datetime
import logging
import time

from doorActionTypes import DoorActionSrc


class Button:
    def __init__(self, app):
        """
        :type app: LockApplicationMain
        """
        self.app = app

    def get_pin_state(self):
        pin_state = None
        if self.app.GPIO:
            pin_state = self.app.GPIO.input(self.app.settings.BUTTON_PIN)
        else:
            f = open("./.states/button_pin", "r")
            pin_state = f.read().strip() == "True"
            f.close()
        return pin_state

    def down(self, reason="Button down", autolock=None, force=None):
        actionSrc = DoorActionSrc.BTN_DOWN
        timeBeforeUnlock = str(datetime.now())
        if force is not None and autolock is None:
            self.app.door.unlock(reason, force=force, src=actionSrc)
        elif autolock is not None and force is None:
            self.app.door.unlock(reason, autolock=autolock, src=actionSrc)
        else:
            self.app.door.unlock(reason, autolock=autolock, force=force, src=actionSrc)
        self.app.sync.sendLive(
            locked=True, eventDate=timeBeforeUnlock, message=reason, src=actionSrc
        )

    def up(self, reason="Button up", force=None):
        actionSrc = DoorActionSrc.BTN_UP
        if force is None:
            self.app.door.lock(reason, src=actionSrc)
        else:
            self.app.door.lock(reason, force=force, src=actionSrc)
        self.app.sync.sendLive(locked=False, message=reason, src=actionSrc)

    def long_down(self, unlocked):
        reason = "Button was long time down"
        actionSrc = DoorActionSrc.BTN_LONG_DOWN
        if unlocked:
            self.app.door.lock(reason, force=True, src=actionSrc)
        else:
            # Allow self.app.door opening in worktime
            if self.app.automation.check_if_worktime(
                datetime.now(), self.app.settings.BUTTON_TRIGGER_ACTIVE_START
            ):
                self.app.door.unlock(reason, autolock=False, force=True, src=actionSrc)
                self.app.sync.sendLive(
                    locked=(not unlocked), message=reason, src=actionSrc
                )
            else:
                logging.info("Door unlocking attempt outside of worktime with button")

    def run(self):
        if self.app.settings.TWO_STATE_BUTTON:
            self.run_twostate()
        else:
            self.run_onestate()

    def run_onestate(self):
        long_down = False
        while True:

            if (
                self.get_pin_state() == self.app.settings.BUTTON_DOWN_STATE
            ):  # pin is low - button is down
                islocked = self.app.door.isLocked()
                logging.debug("Is door currently in locked state: {0}".format(islocked))
                self.down(autolock=True)
                button_down_time = 0

                while self.get_pin_state() == self.app.settings.BUTTON_DOWN_STATE:
                    time.sleep(0.1)
                    button_down_time += 0.1
                    if button_down_time >= self.app.settings.LOCK_BUTTON_LONG_DOWN_TIME:
                        if long_down:
                            continue

                        long_down = True
                        if (
                            self.app.settings.ALLOW_BUTTON_LOCK_TRIGGER
                            and self.app.settings.USE_AUTOMATIC_OPENING
                        ):
                            self.long_down(islocked)
                        else:
                            logging.info(
                                "No action, because button long time down is disabled"
                            )
                if self.app.settings.ALLOW_BUTTON_LOCK_TRIGGER:
                    logging.info(
                        "Button was down {0:.2f} seconds.".format(button_down_time)
                    )
                long_down = False

            time.sleep(0.1)

    def run_twostate(self):
        button_pushed = False
        while True:
            if (
                not button_pushed
                and self.get_pin_state() == self.app.settings.BUTTON_DOWN_STATE
            ):
                self.down(autolock=False, force=True)
                button_pushed = True

            if (
                button_pushed
                and self.get_pin_state() != self.app.settings.BUTTON_DOWN_STATE
            ):
                self.up(force=True)
                button_pushed = False

            time.sleep(1)

    def run_emulation(self):
        while True:
            if self.app.GPIO or not self.app.verbose:
                time.sleep(100000)
            else:
                input("Enter to button down\n")
                self.down()
                self.up()
