import enum
import json
import sqlite3
import threading
import time
import urllib.request
from collections import deque
from datetime import datetime
import logging
from urllib.error import URLError
import ssl

from doorActionTypes import DoorActionTypes

class EventItem():
    def __init__(self, locked=None, personId=None, tagId=None, idcardId = None, eventDate=None, message=None, uid=None, type="INFO", src=None):
        """

        :type src: DoorActionSrc
        """
        self.eventDate = self.now(eventDate)
        self.tagId = self.check(tagId)
        self.personId = self.check(personId)
        self.idcardId = self.check(idcardId)
        self.locked = locked
        self.uid = uid
        self.message = message
        self.type = self.checkEnum(type)
        self.src = self.checkEnum(src)

    def check(self, param):
        if not param:
            return
        return param

    def checkEnum(self, param):
        if not param:
            return
        elif type(param) == str:
            return param
        elif isinstance(param, enum.Enum):
            return param.value
        return

    def now(self, eventDate):
        if eventDate:
            return eventDate
        else:
            return str(datetime.now())

class Sync(threading.Thread):
    def __init__(self, app):
        """
        :type app: LockApplicationMain
        """
        self.app = app
        threading.Thread.__init__(self)
        self.stopped = threading.Event()
        self.savingErrors = []
        self.errorLogSend = False
        self.new_door_action = deque([])
        self.new_door_action_wait_event = threading.Event()
        t = threading.Thread(target=self.clear_door_action_deque)
        t.daemon = True
        t.start()
        self.saveActionDelay = threading.Event()

    def clear_door_action_deque(self):
        while True:
            if self.new_door_action_wait_event.wait():
                self.new_door_action_wait_event.clear()

            contents = []
            while self.new_door_action:
                contents.append(self.new_door_action.popleft())

            if contents:
                for eventItem in contents:
                    self.doorAction(eventItem)
                self.app.sendLive_event.set()

    def run(self):
        while True:
            if self.app.sendLive_event.wait(10):
                self.app.sendLive_event.clear()
            self.sendLiveActions()



    # Workaround until we find better way to send log to server
    def sendLive(
        self,
        locked,
        person=None,
        tag=None,
        eventDate=None,
        message=None,
        uid=None,
        type=DoorActionTypes.INFO,
        src=None,
    ):
        self.new_door_action.append(
            EventItem(
                locked=locked,
                personId=person,
                tagId=tag,
                eventDate=eventDate,
                uid=uid,
                message=message,
                type=type,
                src=src
            )
        )
        self.new_door_action_wait_event.set()

    def doorAction(self, eventItem = EventItem()):

        try:
            self.saveActionDelay.wait(self.app.settings.DOOR_ACTION_SYNC_DELAY)
            conn = sqlite3.connect(self.app.settings.sqlite_file)
            c = conn.cursor()
            o = c.execute(
                """
                INSERT INTO 'users_doorlog'
                    (person_id, tag_id, idcard_id, added, eventDate, locked, uid, message, type, src) 
                VALUES
                     (?, ?, ?, CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?);
                """,
                (eventItem.personId, eventItem.tagId, eventItem.idcardId, eventItem.eventDate, eventItem.locked, eventItem.uid,
                 eventItem.message, eventItem.type, eventItem.src))

            conn.commit()
            conn.close()
            self.savingErrors = []
        except Exception as e:
            if str(e) not in self.savingErrors:
                logging.exception("Failed to log event in database: " + str(e))
                self.savingErrors.append(str(e))

    def sendLiveActions(self, retry=True):
        maxRowId = 0
        data = []
        try:
            conn = sqlite3.connect(self.app.settings.sqlite_file)
            c = conn.cursor()
            o = c.execute(
                "SELECT person_id, tag_id, idcard_id, eventDate, locked, message, id, uid, type, src FROM users_doorlog"
            )
            lines = o.fetchall()
            conn.close()

            for line in lines:
                data.append(
                    {
                        "personId": line[0],
                        "tagId": line[1],
                        "eventDate": line[3],
                        "open": line[4],
                        "message": line[5],
                        "uid": line[7],
                        "type": line[8],
                        "src": line[9],
                    }
                )
                if maxRowId < line[6]:
                    maxRowId = line[6]

            if len(data):
                data = json.dumps(data).encode("utf-8")

                request = urllib.request.Request(
                    url=self.app.settings.SYNC_URL + "sync/actions",
                    data=data,
                    headers={
                        "Accept": "application/json, text/plain, */*",
                        "X-Access-Token": self.app.settings.TOKEN,
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    method="POST",
                )
                with urllib.request.urlopen(request, timeout=10) as resp:
                    self.detectError(resp)
                    logging.debug("Log sent successfully")
                    if maxRowId:
                        self.clearOfflineStorage(maxRowId)
                    self.errorLogSend = False

        except Exception as e:
            logging.error("Failed to send offline log." + str(e))
            self.errorLogSend = True
            if retry:
                self.app.sendLogRetry.run()

    def clearOfflineStorage(self, maxRowId):
        conn = sqlite3.connect(self.app.settings.sqlite_file)
        c = conn.cursor()
        o = c.execute("DELETE FROM users_doorlog WHERE id <= ?;", (maxRowId,))
        conn.commit()
        conn.close()

    def detectError(self, response):
        res_body = response.read()
        if not res_body:
            return
        res = json.loads(res_body.decode("utf-8"))
        if res["result"] == "ERROR":
            msg = "Server responded with error."
            if "code" in res["error"].keys():
                msg += " ({0})".format(res["error"]["code"])
            if "message" in res["error"].keys():
                msg += " " + res["error"]["message"]

            raise Exception(msg)

    def sendUnknownTag(self, tag):
        # TODO: make flow
        return

    def sendLiveNfcTagAddingMode(self, item=None):
        # if not self.app.settings.ALLOW_STATUS_SYNC:
        #     return

        j = json.dumps(item).encode("utf-8")
        request = urllib.request.Request(
            url=self.app.settings.SYNC_URL + "sync/nfcaddingmode",
            data=j,
            headers={
                "Accept": "application/json, text/plain, */*",
                "X-Access-Token": self.app.settings.TOKEN,
                "Content-Type": "application/json; charset=utf-8",
            },
            method="POST",
        )

        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        try:
            with urllib.request.urlopen(request, timeout=10, context=ctx) as resp:
                self.detectError(resp)
        except Exception as e:
            logging.exception("Failed to send new tag info", e)

    # def sendLive(self, new_state, person = None, tag = None, eventDate=str(datetime.now()), message = None, uid = None):
    #     if not self.app.settings.ALLOW_STATUS_SYNC:
    #         return
    #
    #     j = json.dumps({
    #             "eventDate": eventDate,
    #             "personId": person,
    #             "tagId": tag,
    #             "open": new_state,
    #             "message": message,
    #             "uid": uid
    #         }).encode('utf-8')
    #     request = urllib.request.Request(
    #         url =self.app.settings.SYNC_URL + "sync/action",
    #         data= j,
    #         headers= {
    #             "Accept": "application/json, text/plain, */*",
    #             "X-Access-Token": self.app.settings.TOKEN,
    #             "Content-Type":"application/json; charset=utf-8"
    #         },
    #         method="POST",
    #     )
    #
    #     try:
    #         with urllib.request.urlopen(request) as resp:
    #             self.detectError(resp)
    #             self.sendLiveActions()
    #     except Exception as e:
    #         logging.debug("Stored data into database")
    #         self.doorAction(locked=new_state, person=person, tag=tag, eventDate=eventDate, uid=uid, message=message)
    def sendRemoteOpenEnd(self):
        j = json.dumps(
            {
                "newTime": None,
                "pw": "mingitoken",
            }
        ).encode("utf-8")

        request = urllib.request.Request(
            url=self.app.settings.SYNC_URL_EXTRA + "remoteOpenEnd.php",
            data=j,
            headers={
                "Accept": "application/json, text/plain, */*",
                "X-Access-Token": self.app.settings.TOKEN,
                "Content-Type": "application/json; charset=utf-8",
            },
            method="POST",
        )

        try:
            with urllib.request.urlopen(request) as resp:
                self.detectError(resp)
                return True
        except Exception as e:
            logging.debug("Sending remote open until end failed.")
