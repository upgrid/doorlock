#!/usr/bin/env bash

LOCK_PIN=20
BUTTON_PIN=12
SECOND_WIEGAND_TRIGGER_PIN=10

create_open_close  () {

cat > close.sh << EOF
#!/bin/bash

#Lock door
pinctrl set $LOCK_PIN op pn dl

EOF

cat > open.sh << EOF
#!/bin/bash

#Unlock door
pinctrl set $LOCK_PIN op pn dh

EOF

chmod +x open.sh
chmod +x close.sh
}

# Create open.sh and close.sh , add it into crontab
echo "Do you wan't create open.sh and close.sh?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) create_open_close ; break;;
        No ) break;;
    esac
done

setup_launchers() {
    mkdir bin

    cat > bin/doorlock.sh << EOF
#!/bin/sh

cd $PWD
python3 main.py

EOF

    cat > bin/doorlock-web.sh << EOF
#!/bin/sh

cd $PWD/web
python3 manage.py runserver 0.0.0.0:8080

EOF

    cat > debug.sh << EOF
#!/bin/bash

#Kill previous controller program
sudo service 2-doorlock stop

#Open door controller fast locking, verbose, debug mode
cd $PWD
python3 main.py -d -v -f

EOF




    sudo tee /etc/systemd/system/2-doorlock.service > /dev/null << EOF
[Unit]
Description=Doorlock service
Requires=pcscd.service pigpiod.service
After=pcscd.service pigpiod.service

[Service]
ExecStart=$PWD/bin/doorlock.sh
Type=idle
User=$USER

[Install]
WantedBy=multi-user.target

EOF



    sudo tee /etc/systemd/system/2-doorlock-web.service > /dev/null << EOF
[Unit]
Description=Doorlock web admin

[Service]
ExecStart=$PWD/bin/doorlock-web.sh
User=$USER

[Install]
WantedBy=multi-user.target

EOF

    chmod +x bin/doorlock.sh
    chmod +x bin/doorlock-web.sh
    chmod +x debug.sh

    sudo gpasswd -a $USER systemd-journal

    # Wiegand reading depends on pigpiod
    sudo systemctl enable pigpiod.service
}


echo "Do you wan't create launchers for systemd autostart, debug?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) setup_launchers ; break;;
        No ) break;;
    esac
done

echo "Do you wan't to autostart 2-doorlock.service?"
select yn in "Yes" "No" "Nothing"; do
    case $yn in
        Yes ) sudo systemctl enable 2-doorlock.service; break;;
        No ) sudo systemctl disable 2-doorlock.service; break;;
        Nothing) break;;
    esac
done

echo "Do you wan't to autostart 2-doorlock-web.service?"
select yn in "Yes" "No" "Nothing"; do
    case $yn in
        Yes ) sudo systemctl enable 2-doorlock-web.service; break;;
        No ) sudo systemctl disable 2-doorlock-web.service; break;;
        Nothing) break;;
    esac
done

setup_wiegand_settings () {
        cat >> settings_local.py << EOF
TWO_WIEGAND_READERS = True
SECOND_WIEGAND_TRIGGER_PIN = $SECOND_WIEGAND_TRIGGER_PIN
SECOND_WIEGAND_BOOLEAN = True

DOOR_PIN = $LOCK_PIN
BUTTON_PIN = $BUTTON_PIN

READER_TYPES = ["WIEGAND"]
EOF
}

setup_wiegand_settings