import logging
import os
import subprocess
import threading
import time
import urllib.request


class SendLogRetry:
    def __init__(self, app):
        super().__init__()
        self.app = app

    def _canReachToServer(self):
        logging.debug("Pinging: " + self.app.SYNC_URLhostName)
        try:
            p = subprocess.Popen(
                ["ping", self.app.SYNC_URLhostName, "-c 1"],
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            )
            response = str(p.stdout.read())
            if "min/avg/max/" in response:
                return True
            else:
                return False
        except Exception:
            return None

    # noinspection PyUnreachableCode
    def _canReachApiApp(self):
        if self.app.settings.LOG_SEND_RETRY_DOES_API_ALIVE_CHECK:
            logging.debug("Doing api alive check")
            request = urllib.request.Request(
                url=self.app.settings.SYNC_URL + "sync/isapialive",
                headers={
                    "Accept": "application/json, text/plain, */*",
                    "X-Access-Token": self.app.settings.TOKEN,
                    "Content-Type": "application/json; charset=utf-8",
                },
                method="GET",
            )

            try:
                with urllib.request.urlopen(request, timeout=10) as resp:
                    res_body = resp.read()
                    rs = res_body.decode("utf-8")
                    logging.debug("Api is alive: " + rs)
                    return True
            except Exception as e:
                logging.debug("Api alive check failed: " + str(e))
                return False

        logging.debug("LOG_SEND_RETRY_DOES_API_ALIVE_CHECK is disabled.")
        return True

    def run(self):
        logging.debug(
            "Door log sending retry started with {0} seconds interval.".format(
                self.app.logSendRetryInteval
            )
        )
        while True:
            time.sleep(self.app.logSendRetryInteval)
            try:
                if self._canReachToServer() and self._canReachApiApp():
                    self.app.sendLive_event.set()
                    break
            except Exception as e:
                logging.error("Exception during log send retry: " + str(e))


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.DEBUG, format="%(asctime)s | %(levelname)s | %(message)s"
    )
    from main import LockApplicationMain
    import settings
    settings.LOG_SEND_RETRY_DOES_API_ALIVE_CHECK = True

    app = LockApplicationMain(settings, full=True)
    print("Ping:", app.sendLogRetry._canReachToServer())
    print("Api:", app.sendLogRetry._canReachApiApp())
    settings.SYNC_URL = "http://test.upgrid.dynu.net:5003/"
    app = LockApplicationMain(settings, full=True)
    print("Ping", app.sendLogRetry._canReachToServer())
    print("Api", app.sendLogRetry._canReachApiApp())
