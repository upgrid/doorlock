#! /usr/bin/env python3


from __future__ import print_function
from time import sleep
import signal
import json
from smartcard.CardMonitoring import CardMonitor, CardObserver
from smartcard.util import toHexString, toBytes
import os.path

filename = "tags.csv"
# a simple card observer that tries to select DF_TELECOM on an inserted card
class selectDFTELECOMObserver(CardObserver):
    """A simple card observer that is notified
    when cards are inserted/removed from the system and
    prints the list of cards
    """

    def __init__(self):
        new = True
        if os.path.isfile(filename):
            new = False
        self.file = open(filename, "a")
        if new:
            self.file.write("UID, Name\n")

    # Handle ctrl-c
    def sigint_handler(self, signum, frame):
        cardmonitor.deleteObserver(selectobserver)
        self.file.close()
        print()
        exit()

    @staticmethod
    def card_action(data_unit, card, decode=False):
        (response, sw1, sw2) = card.connection.transmit(toBytes(data_unit))
        if sw1 == 97:  # Card sends selected bytestring lenght
            (response, sw1, sw2) = card.connection.transmit(
                toBytes("00C00000" + toHexString([sw2]))
            )
        if sw1 != 144:  # Not OK
            raise Exception(
                "Response status is: %x %x used adpu: %s" % (sw1, sw2, data_unit)
            )
        if decode:
            if response[0] in [0, 32]:  # Check if response is empty
                return ""
            return (
                bytearray(response)
                .decode(encoding="ISO 8859-1", errors="strict")
                .strip()
            )
        else:
            return response

    def update(self, observable, actions):
        (addedcards, removedcards) = actions
        for card in addedcards:
            if (
                "Contactless" in card.reader
                or "ACS" in card.reader
                and "SAM" not in card.reader
            ):

                card.connection = card.createConnection()
                card.connection.connect()
                response = toHexString(self.card_action("FFCA000000", card))
                print("UID: " + response)
                name = input("Name: ").strip()
                line = response + ", " + name.title() + "\n"
                if name:
                    self.file.write(line)
                print("Waiting for next tag.\n")


if __name__ == "__main__":

    print("Scan NFC Tag")
    print("This program will run forever or until you use Ctrl + C")
    print("")

    cardmonitor = CardMonitor()
    selectobserver = selectDFTELECOMObserver()
    cardmonitor.addObserver(selectobserver)

    signal.signal(signal.SIGTERM, selectobserver.sigint_handler)
    signal.signal(signal.SIGINT, selectobserver.sigint_handler)

    while True:
        sleep(180)

    import sys

    if "win32" == sys.platform:
        print("press Enter to continue")
        sys.stdin.read(1)
