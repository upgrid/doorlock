ALLOW_BUTTON = True
LOCKED_PIN_STATE = False
BUTTON_DOWN_STATE = False

USE_AUTOMATIC_OPENING = False
LIMIT_ACTIVE_TIME = False
ALLOW_REMOTE_OPEN = True
ALLOW_REMOTE_CLOSE = True
ALLOW_REMOTE_OPEN_WITH_TIME = False
ADD_UNKNOWN_TAG_TO_DATABASE = False
ADD_UNKNOWN_IDCARD_TO_DATABASE = False
ALLOW_BUTTON_LOCK_TRIGGER = False
FALLBACK_TO_IDCODE_CHECK = False

ALLOW_STATUS_SYNC = True
ALLOW_DATABASE_SYNC = True
LOG_SEND_RETRY_DOES_API_ALIVE_CHECK = True

LOCK_OPEN_TIME = 5
LOCK_BUTTON_LONG_DOWN_TIME = 2
DOOR_ACTION_SYNC_DELAY = 2
TWO_STATE_BUTTON = False

WORKDAY_START = "8:00:00"
WORKDAY_END = "18:00:00"
WORKDAYS = [0, 1, 2, 3, 4]
SHORTER_WORKDAY = ["1.1", "24.2", "23.6", "24.12"]
SHORTER_WORKDAY_END = "15:00:00"

SYNC_URL = "https://upgrid.dynu.net/api/"
TOKEN = "DEMO"

if LIMIT_ACTIVE_TIME:
    ACTIVE_START = "11:00:00"
    ACTIVE_END = "22:00:00"

BUTTON_TRIGGER_ACTIVE_START = "6:00:00"

WIEGAND = [17, 18]

from datetime import datetime

try:
    import RPi.GPIO as GPIO
except Exception:
    GPIO = False

# configuration constants
door_id = "test1"

# Database location and table
sqlite_file = "web/db.sqlite3"
REMOTE_OPEN_FILE = ".states/remoteOpen"
REMOTE_OPEN_FILE_END = ".states/remoteOpenEnd"
UPDATE_MARKER_FILE = "./.states/updateMarker"
SYNC_URL_EXTRA = "https://upgrid.dynu.net/remoteOpenClose/"
EMPTY_LIST_SYNCROM_SHIFT = 5 * 60
EMPTY_LIST_SYNCROM_SHIFT_COUNT = 3

# RPI_REVISION :  0 = Compute Module, 1 = Rev 1, 2 = Rev 2, 3 = Model B+/A+
if GPIO:
    if GPIO.RPI_REVISION >= 2:
        DOOR_PIN = 21
        BUTTON_PIN = 13
    else:
        DOOR_PIN = 7
        BUTTON_PIN = 10


LED_PIN = 20
INVERTED_LED = False


HOLIDAYS = {
    2023: ['1.1', '24.2', '7.4', '9.4', '1.5', '28.5', '23.6', '24.6', '20.8', '24.12', '25.12', '26.12'],
    2024: ['1.1', '24.2', '29.3', '31.3', '1.5', '19.5', '23.6', '24.6', '20.8', '24.12', '25.12', '26.12'],
    2025: ['1.1', '24.2', '18.4', '20.4', '1.5', '8.6', '23.6', '24.6', '20.8', '24.12', '25.12', '26.12'],
    2026: ['1.1', '24.2', '3.4', '5.4', '1.5', '24.5', '23.6', '24.6', '20.8', '24.12', '25.12', '26.12'],
    2027: ['1.1', '24.2', '26.3', '28.3', '1.5', '16.5', '23.6', '24.6', '20.8', '24.12', '25.12', '26.12'],
    2028: ['1.1', '24.2', '14.4', '16.4', '1.5', '4.6', '23.6', '24.6', '20.8', '24.12', '25.12', '26.12'],
}


# 2020/2021 aastavahetus
#'2020' : ['1.1', '2.1', '3.1', '4.1', '5.1', '6.1', '7.1', '8.1', '9.1', '10.1'],
#'2021' : ['23.12','24.12','25.12','26.12','27.12','28.12','29.12','30.12','31.12']

UNLIMITED_GROUPS = []
NFC_ADDING_MODE_COUNT = 1

TWO_WIEGAND_READERS = False
SECOND_WIEGAND_TRIGGER_PIN = 10
SECOND_WIEGAND_BOOLEAN = False

READER_TYPES = ["WIEGAND", "PYSCARD"]

try:
    from settings_local import *
except ImportError:
    try:
        from .settings_local import *
    except SystemError:
        pass
    except ImportError:
        pass


LOCKED_TIMINGS = {
    "workdays": WORKDAYS,
    "workday_start": datetime.strptime(WORKDAY_START, "%H:%M:%S").time(),
    "workday_end": datetime.strptime(WORKDAY_END, "%H:%M:%S").time(),
    "shorter_workday": SHORTER_WORKDAY,
    "shorter_workday_end": datetime.strptime(SHORTER_WORKDAY_END, "%H:%M:%S").time(),
}

BUTTON_TRIGGER_ACTIVE_START = datetime.strptime(
    BUTTON_TRIGGER_ACTIVE_START, "%H:%M:%S"
).time()


if LIMIT_ACTIVE_TIME:
    ACTIVE_TIME = {
        "start": datetime.strptime(ACTIVE_START, "%H:%M:%S").time(),
        "end": datetime.strptime(ACTIVE_END, "%H:%M:%S").time(),
    }
