#!/usr/bin/env bash

cat >> settings_local.py << EOF
TWO_WIEGAND_READERS = True
SECOND_WIEGAND_TRIGGER_PIN = 10
SECOND_WIEGAND_BOOLEAN = False
EOF

sudo service doorlock restart
sudo service doorlock-web restart