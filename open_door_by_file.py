from time import time

from watchdog.events import FileSystemEventHandler
from datetime import datetime
import logging
import threading


class OpenByFile(FileSystemEventHandler):
    def __init__(self, app):
        """
        :type app: LockApplicationMain
        """
        self.app = app
        super().__init__()
        self.timer = None

    def on_modified(self, event):
        if event.src_path == "./" + self.app.settings.REMOTE_OPEN_FILE:
            self.handleRemoteOpen()

    def handleRemoteOpen(self, init=False):
        try:
            f = open(self.app.settings.REMOTE_OPEN_FILE, "r")
            r = f.read().strip()
            f.close()
            if r:
                if int(r) < int(time()):
                    logging.info(
                        "Removing passed remote open until date: {}".format(
                            datetime.fromtimestamp(int(r))
                        )
                    )
                    self.clearRemoteOpenFile()
                    self.app.openByFileEnd.saveRemoteOpenFileEnd()
                else:
                    self.setNewRemoteEndTime(int(r), init)

            else:
                if self.app.remoteOpenEndTime or self.timer:
                    logging.info(
                        "Remote open until {0} will be canceled.".format(
                            datetime.fromtimestamp(self.app.remoteOpenEndTime)
                        )
                    )
                    self.setNewRemoteEndTime(None, init)
                    self.cancelTimer()

        except Exception as e:
            logging.exception(e)

    def clearRemoteOpenFile(self):
        f = open(self.app.settings.REMOTE_OPEN_FILE, "w")
        f.write("")
        f.close()

    def onRemoteOpenUntilTimeEnd(self):
        now = datetime.now().replace(microsecond=0)
        if not self.app.automation.check_if_worktime(now):
            self.setNewRemoteEndTime(None)
            self.app.door.lock("Remote open ended", force=True)
            self.app.openByFileEnd.saveRemoteOpenFileEnd()

    def setTimer(self, remoteOpenEndTime):
        if self.timer:
            self.cancelTimer()
        newTime = int(remoteOpenEndTime) - int(time())
        if newTime > 0:
            self.timer = threading.Timer(newTime, self.onRemoteOpenUntilTimeEnd)
            self.timer.start()

    def cancelTimer(self):
        if self.timer:
            self.timer.cancel()
        self.timer = None

    def setNewRemoteEndTime(self, remoteOpenEndTime, init=False):
        if remoteOpenEndTime != self.app.remoteOpenEndTime:
            if remoteOpenEndTime:
                if not self.app.automation.check_if_worktime(
                    datetime.fromtimestamp(remoteOpenEndTime)
                ):
                    logging.info(
                        "New remote open until set to {0}".format(
                            datetime.fromtimestamp(remoteOpenEndTime)
                        )
                    )
                    self.app.remoteOpenEndTime = remoteOpenEndTime
                    self.setTimer(remoteOpenEndTime)
            else:
                self.cancelTimer()
                self.app.remoteOpenEndTime = None
                self.clearRemoteOpenFile()
