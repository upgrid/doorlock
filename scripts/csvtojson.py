import csv
import json

import itertools

csvfile = reversed(list(csv.DictReader(open('tabel.csv', 'r'))))
output = []

names = {}
stickers = []

wokplaces = dict()

def tag(uid, name):
    return {
        "uid": uid.upper(),
        "name": name.title()
    }

def mapGroup(workplace = "", role = ""):
    if  workplace == "RateChain Solutions":
        return "Ratechain"
    if role.lower() == "admin":
        return "Admin"
    if workplace.title() in ["Pipedrive", "Contriber", "Evocon", "Koristajad", "Upgrid", "Vassilissa", "Fractory", "Helmes", "Carto"]:
        return workplace.title()
    if "hot desk" in role.lower():
        return "Hot-desk member"
    if workplace in ["Deckol (majahalduri tööline)", "töömees", "töömees (Madise tiim)", "Torumeeste meeskonnast", "Startupday töömees", "elektrik", "ehitus", "Tehnohooldus"]:
        return "Töömehed"
    if workplace == "MatchMySound":
        return "Match my sound"
    if workplace == "Marti Nurmine koostööpartner":
        return "Admin"
    if workplace in ["ISS", "koristaja", "vaibapuhastaja"]:
        return "Koristajad"
    if workplace == "Vendimees":
        return "Vendimees"

    return "Resident member"
for l in csvfile:

     if l["Värv"] == "#ff0000":
        continue

     name = l["First name"] + " " + l["Family name"]
     np = name + " " + l["Workplace"]
     pointer = 0

     if not np in names.keys():
         names[np]= len(output)
     else:
         pointer = names[np]

     if  l["Workplace"] in wokplaces.keys():
         wokplaces[l["Workplace"]] += 1
     else:
         wokplaces[l["Workplace"]] = 1
     tags = []

     if l["NFC card UID"]:
         tags.append(tag(l["NFC card UID"], l["NFC card name"]))

     if l["Card 2"]:
         tags.append(tag(l["Card 2"],l["Card 2 name"]))

     if l["C3"]:
         tags.append(tag(l["C3"],l["C3 Name"]))


     if l["NFC Tag UID"]:
         if l["NFC tag number"] in stickers:
             print(name," | ",  l["NFC Tag UID"]," | " , l["NFC tag number"])
             continue
         else:
             stickers.append(l["NFC tag number"])
             tags.append(tag(l["NFC Tag UID"], "NFC Tag nr " + l["NFC tag number"]))




     if pointer:
         output[pointer]["tags"] += tags
     else:
         output.append({
             "name": name,
             "workplace": l["Workplace"],
             "tags": tags,
             "role": l["Role"],
             "email": l["Email address"],
             "phone": l["Phone number"],
             "group": mapGroup(l["Workplace"], l["Role"]),
             "groups": []
     })

print(wokplaces)

for l in output:
    print(l["workplace"], l["group"], l["role"], sep="|")
json.dump(output, open('tabel.json', 'w'), indent=4, sort_keys=False)
I