#!/usr/bin/env bash
cd doorlock
cp settings.py settings_local.py
git checkout .
git pull
sudo pip3 install pyscard --upgrade
sudo rm /etc/cron.weekly/restart-doorlock
sudo service doorlock restart
sudo service doorlock web-restart