#Koolitus 3.06 - 6.06 ajutine avamine 8:00 - 20:30
RULEDAYS = ["3-06-2021", "4-06-2021", "5-06-2021", "6-06-2021"]

from datetime import datetime
now = datetime.now().replace(microsecond=0).date()
ruledates = [datetime.strptime(ruleday, '%d-%m-%Y').date() for ruleday in RULEDAYS]

if now in ruledates:
    USE_AUTOMATIC_OPENING = True
    WORKDAY_START = '0:00:00'
    WORKDAY_END = '23:59:59'
    WORKDAYS = [now.weekday()]


def check(day, strpTime=True):
    if strpTime:
        now = datetime.strptime(day, '%d-%m-%Y').date()
    else:
        now = day
    print(now, now.weekday())
    if now in ruledates:
        print(day, "OK")
    else:
        print(day, "OUT")

for ruleday in RULEDAYS:
    check(ruleday)

check("2-06-2021")
check(now, False)