#!/usr/bin/env bash

logfile="log-ssh_exec.log"
project_foler="~/doorlock"
s_file="$project_foler/settings.py"
db_file="$project_foler/web/db.sqlite3"


>$logfile

function ssh_exec() {
    for f in $(cat seadmed);
    do
        ssh pi@$f.local "$1"  >> $logfile
    done
}



cat > $logfile << EOF
########
hostname
ip
mac
WORKDAY_START
WORKDAY_END
workdays
LOCKED_PIN_SATE
branch
Commit
Commit message
Commit time
Tags in database
ATIVE_START
ACTIVE_END
Pyscard version
EOF

doors_exec "
    echo '#######';
    echo \$HOSTNAME;
    ip addr show eth0 | awk -F'/|inet ' '/inet / {print $2}';
    ip link show eth0 | awk '/ether/ {print $2}';
    awk -F'=|= ' '/^WORKDAY_START/{print \$2}' $s_file;
    awk -F'=|= ' '/^WORKDAY_END/{print \$2}' $s_file;
    awk -F':|: ' '/^    \"workdays\"/{print \$2}' $s_file;
    awk -F'=|= ' '/^LOCKED_PIN_STATE/{print \$2}' $s_file;
    cd $project_foler ; git name-rev --name-only HEAD;
    cd $project_foler ; git log --pretty=format:'%h%n' -1;
    cd $project_foler ; git log --pretty=format:'%s%n' -1;
    cd $project_foler ; git log --pretty=format:'%cr%n' -1;
    sqlite3 $db_file 'select * from users_tag' | wc -l
    awk -F'=|= ' '/^ACTIVE_START/{print \$2}' $s_file;
    awk -F'=|= ' '/^ACTIVE_END/{print \$2}' $s_file;
    pip3 freeze | awk -F'==' '/pyscard/{print\$2}';
"
