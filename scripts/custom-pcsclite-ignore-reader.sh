#!/usr/bin/env bash

pcsclite_version="1_1.8.20-2"
tmp_dir="/tmp/ignore_reader"_$pcsclite_version
zipfile=valdur-pcsc-lite-$pcsclite_version.zip

mkdir $tmp_dir
cd $tmp_dir || exit
wget http://10.10.99.21:5003/custom-libpcsclite/$zipfile || wget http://test.upgrid.dynu.net:5003/custom-libpcsclite/$zipfile
unzip $zipfile
sudo dpkg -i *.deb

sudo tee  /etc/default/pcscd > /dev/null << EOF
PCSCLITE_FILTER_IGNORE_READER_NAMES="R502 SAM1 Reader:R502 Contact Reader"
EOF
sudo service pcscd restart

cd ..
rm -rf $tmp_dir

python3 -c "from smartcard.System import readers; print(readers())"