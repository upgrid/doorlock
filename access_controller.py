import logging
import sqlite3
from datetime import datetime
from string import Template

from doorActionTypes import DoorActionTypes, DoorActionSrc
from person import PersonAuthData


class AccessController:
    def __init__(self, app):
        """
        :type app: LockApplicationMain
        """
        self.app = app

        # Only usage for those vars are for nfc tag adding mode
        self.nfc_count = 0
        self.tagAddingModeTagUid = ""
        self.lastPersonWithTag = PersonAuthData()
        self.lastPersonWithIdCard = PersonAuthData()
        self.actionSrc = DoorActionSrc.CARD

    def clear_nfc_vars(self, clearCount=False):
        self.tagAddingModeTagUid = ""
        if clearCount:
            self.nfc_count = 0
        self.lastPersonWithTag = PersonAuthData()

    def clear_idcard_vars(self):
        self.lastPersonWithIdCard = PersonAuthData()

    def clear_all(self):
        self.clear_idcard_vars()
        self.clear_nfc_vars(clearCount=True)

    def clear_tag(self):
        if self.lastPersonWithTag.tag.uid and self.nfc_count == 0:
            self.clear_nfc_vars()

    def get_nfc_data(self, tagUid):
        conn = sqlite3.connect(self.app.settings.sqlite_file)
        now = str(datetime.now().time().strftime("%H:%M:%S"))
        # 1 = Monday, ... 7 = Sunday converting to 0 because of admin has 0 as sunday
        weekday = datetime.now().isoweekday()
        if weekday == 7:
            weekday = 0
        c = conn.cursor()
        sql = Template(
           f"""
            SELECT
              MAX(active) as active,
              name,
              id,
              person_id,
              is_unlimited_group
            FROM (SELECT DISTINCT
                    MIN(
                        ut.active, 
                        COALESCE(up.active,1), 
                        COALESCE(CURRENT_TIMESTAMP > up.unactive_until,1),
                        CASE (
                            SELECT * FROM (SELECT 1 FROM users_grouplimit WHERE gr_limit.group_id = gr_members.group_id) as s
                        ) WHEN 1 
                        THEN (
                            (SELECT :nowTime BETWEEN COALESCE(gr_limit.from_time, '00:00:00') AND COALESCE(gr_limit.to_time, '24:00:00')) 
                            AND gr_limit.weekday = :weekDay) 
                        ELSE 1 END
                    ) AS active, 
                    COALESCE(up.name,'') as name,
                    ut.id,
                    ut.person_id,
                    CASE (
                        SELECT * FROM (
                            SELECT 1 
                            FROM users_group_members 
                            WHERE 
                                gr_members.group_id = users_group_members.group_id 
                                AND gr_members.group_id IN ({','.join(map(str, self.app.settings.UNLIMITED_GROUPS))})
                            ) as x) 
                    WHEN 1 
                        THEN 1 
                        ELSE 0 
                    END as is_unlimited_group
                FROM users_tag ut
                LEFT JOIN users_person up ON up.id = ut.person_id
                LEFT JOIN users_group_members gr_members on up.id = gr_members.person_id
                LEFT JOIN users_grouplimit gr_limit on gr_members.group_id = gr_limit.group_id
            WHERE $addtionalWhere ) as k 
            GROUP BY id;
                """
        )

        mainParams = {
            "weekDay": weekday,
            "nowTime": now,
        }

        if len(tagUid) > 8:
            params = {"uid": tagUid} | mainParams
            c.execute(
                sql.substitute(
                    addtionalWhere="uid = :uid",
               ),
                params
            )
        else:
            params = {
                         "uid1": tagUid + "%",
                         "uid2": "04 " + tagUid + "%",
                         "uid3": "A7 " + tagUid + "%",
                     } | mainParams
            c.execute(
                sql.substitute(
                    addtionalWhere="uid LIKE :uid1 OR uid LIKE :uid2 OR uid LIKE :uid3",
                ),
                params
            )
        resp = c.fetchone()
        conn.close()
        if resp:
            return PersonAuthData(
                active=resp[0],
                personName=resp[1],
                tagUid=tagUid,
                tagId=resp[2],
                personId=resp[3],
                unlimitGroup=resp[4],
            )
        else:
            return PersonAuthData(tagUid=tagUid)

    def accept_nfc(self, tag, normal=True):
        # Check if we are in nfc adding mode.
        if normal:
            if self.nfc_tag_adding_mode(tag):
                return False

        try:
            person = self.get_nfc_data(tag)
            return self.auth_nfc_data(person, normal)
        except Exception as e:
            logging.exception(e)
            return None

    def auth_nfc_data(self, person, normal):
        if person.tag.id:
            if normal and (not person.active or not self.is_unlock_time_limit(person)):  # Card is not activated
                logging.warning(
                    f"Unactive tag or person {person.tag.uid} {person.name}"
                )
                self.app.sync.sendLive(
                    locked=None,
                    person=person.id,
                    tag=person.tag.id,
                    uid=person.tag.uid,
                    type=DoorActionTypes.UNACTIVE_TAG_OR_PERSON,
                    src=self.actionSrc,
                )
                return False

            self.app.sync.sendLive(
                locked=True,
                person=person.id,
                tag=person.tag.id,
                uid=person.tag.uid,
                type=DoorActionTypes.UNLOCK,
                src=self.actionSrc,
            )
            self.app.addLastLog(person.id)
            self.lastPersonWithTag = person
            return True
        else:
            if self.nfc_count == 0:
                logging.warning(f"Unknown tag: {person.tag.uid} ")
                self.app.sync.sendLive(
                    locked=None,
                    uid=person.tag.uid,
                    type=DoorActionTypes.UNKNOWN_CARD,
                    src=self.actionSrc,
                )
                self.clear_nfc_vars()
        return False

    def accept_person(self, idcode):
        accept, read_data = (None, None)
        try:
            person = self.get_person_data(idcode)
            accept, read_data = self.auth_person_data(person)
            self.lastPersonWithIdCard = person
        except Exception as e:
            logging.exception(e)

        return accept, read_data

    def auth_person_data(self, person: PersonAuthData):
        accept, read_data = (False, False)

        if person.id:
            if person.active:  # Person is activated
                accept = True
            else:
                logging.warning(f"Unactive person {person.idCard.idCode} {person.name}")
        else:
            logging.warning(f"Unknown person: {person.name} {person.idCard.idCode}")
            read_data = True

        return accept, read_data

    def get_person_data(self, idcode):
        conn = sqlite3.connect(self.app.settings.sqlite_file)
        c = conn.cursor()
        c.execute(
            """
                      SELECT 
                        id,
                        MIN(
                          active, 
                          COALESCE(CURRENT_TIMESTAMP > up.unactive_until,1)
                        ) AS active,
                        COALESCE(up.name,'') as name
                      FROM users_person up
                      WHERE idcode = ?
                      """,
            (idcode,),
        )
        resp = c.fetchone()
        conn.close()
        person = PersonAuthData(idCode=idcode)
        if resp:
            person = PersonAuthData(
                personId=resp[0], active=resp[1], personName=resp[2]
            )
        return person

    def accept_id(self, document_nr):
        try:
            person = self.get_idcard_data(document_nr)
            accept, read_data = self.auth_idcard_data(person)
            self.lastPersonWithIdCard = person

        except Exception as e:
            logging.exception(e)
            accept, read_data = (None, None)

        return accept, read_data

    def auth_idcard_data(self, person: PersonAuthData):
        accept, read_data = (False, False)
        if person.idCard.id:
            if person.active:  # Card is activated
                accept = True
            else:
                logging.warning(
                    f"Unactive card or person {person.idCard.cardNr} {person.name}"
                )
        else:
            logging.info(f"Unknown idcard: {person.idCard.cardNr}")
            read_data = True
        return accept, read_data

    def get_idcard_data(self, document_nr):
        conn = sqlite3.connect(self.app.settings.sqlite_file)
        c = conn.cursor()
        c.execute(
            """
                    SELECT 
                        person_id, 
                        MIN(
                            ui.active, 
                            COALESCE(up.active, 1),
                            COALESCE(CURRENT_TIMESTAMP > up.unactive_until, 1)
                        ) AS active,
                        COALESCE(up.name,'') as name,
                        ui.id as idCard_id
                    FROM users_idcard ui 
                    LEFT JOIN users_person up ON up.id = ui.person_id
                    WHERE document = ?
                    """,
            (document_nr,),
        )
        resp = c.fetchone()
        conn.close()
        if resp:
            return PersonAuthData(
                personId=resp[0],
                active=resp[1],
                personName=resp[2],
                idCardNr=document_nr,
                idCardId=resp[3],
            )
        else:
            return PersonAuthData(idCardNr=document_nr)

    def nfc_tag_adding_mode(self, nfc_uid):
        # User has inserted known ID-card and nfc tag has not touched reader 3 times
        if self.lastPersonWithIdCard.idCard.id and self.nfc_count < 3:
            self.nfc_count += 1
            if not self.accept_nfc(
                    nfc_uid, normal=False
            ):  # Check if tag is already in base
                if self.nfc_count == 1:
                    logging.info(">> New NFC tag adding mode")
                    self.tagAddingModeTagUid = nfc_uid

                if self.tagAddingModeTagUid == nfc_uid:
                    logging.info(f"NFC add attempt: {self.nfc_count}")
                    if self.nfc_count == 3:
                        logging.info(
                            f"System will add nfc tag with UID: {nfc_uid}"
                        )
                        self.app.sync.sendLiveNfcTagAddingMode(
                            {
                                "name": self.lastPersonWithIdCard.name,
                                "idcode": self.lastPersonWithIdCard.idCard.idCode,
                                "document_nr": self.lastPersonWithIdCard.idCard.cardNr,
                                "tags": [
                                    {"uid": self.tagAddingModeTagUid, "name": "rmtk"}
                                ],
                                "groups": [],
                            }
                        )
                else:
                    logging.error("User added different NFC tag")
                    self.clear_nfc_vars(clearCount=True)
            else:
                logging.info(
                    f"Can't add {nfc_uid} into database, because it is already in there"
                )
                self.clear_nfc_vars(clearCount=True)
            return True

        return False

    def is_unlock_time_limit(self, person: PersonAuthData):
        if self.app.settings.LIMIT_ACTIVE_TIME:
            if person.unlimitedGroup:
                return True

            now = datetime.now().replace(microsecond=0)
            if (
                    self.app.settings.ACTIVE_TIME["start"]
                    <= now.time()
                    < self.app.settings.ACTIVE_TIME["end"]
            ):
                return True
            else:
                return False

        return True

