import logging
from datetime import datetime, timedelta, date
from time import time
import threading


class Automatic(threading.Thread):
    def __init__(self, app):
        """
        :type app: LockApplicationMain
        """
        self.app = app
        threading.Thread.__init__(self)
        self.stopped = threading.Event()
        self.lastlock = None
        self.lastopen = None
        self.remoteOpenEnd = None

    def run(self):
        if self.app.settings.USE_AUTOMATIC_OPENING:
            self.check(init=True)

        # TODO: Bigger poll time
        while not self.stopped.wait(10):
            self.check()

    def pauseCheck(self):
        self.stopped.set()

    def resumeCheck(self):
        self.stopped.clear()

    def setNewLastLock(self, now=None):
        self.lastlock = self.nowDay(now)
        self.lastopen = None

    def setNewLastOpen(self, now=None):
        self.lastopen = self.nowDay(now)
        self.lastlock = None

    def nowDay(self, now):
        if not now:
            now = datetime.now().replace(microsecond=0)
        return now.day

    @staticmethod
    def day_str(raw):
        return "{0}.{1}".format(raw.day, raw.month)

    @staticmethod
    def time_str(raw):
        return raw.strftime("%H:%M")

    def get_workday_end(self, now):
        workday_end = self.app.settings.LOCKED_TIMINGS["workday_end"]
        next_day = now + timedelta(days=1)
        if (
            self.day_str(next_day)
            in self.app.settings.LOCKED_TIMINGS["shorter_workday"]
        ):
            workday_end = self.app.settings.LOCKED_TIMINGS["shorter_workday_end"]
        return workday_end

    def check_if_worktime(self, now=None, worktime_start=None):
        if self.app.settings.USE_AUTOMATIC_OPENING:
            if not now:
                now = datetime.now().replace(microsecond=0)

            if not worktime_start:
                worktime_start = self.app.settings.LOCKED_TIMINGS["workday_start"]

            workday = now.weekday() in self.app.settings.LOCKED_TIMINGS["workdays"]
            if not workday:
                return False

            holiday = False
            if str(now.year) in self.app.settings.HOLIDAYS.keys():
                holiday = self.day_str(now) in self.app.settings.HOLIDAYS[str(now.year)]
            if holiday:
                return False

            worktime = worktime_start <= now.time() < self.get_workday_end(now)
            return worktime
        else:
            return False

    def check(self, init=False):
        if self.app.remoteOpenEndTime:
            return

        now = datetime.now().replace(microsecond=0)
        if self.check_if_worktime(now):
            if self.lastopen == now.day:
                return

            message = "Woktime start"
            if init:
                message = "Init inside of worktime"

            self.app.door.unlock(message, autolock=False, force=True, src="AUTOMATION")
            self.setNewLastOpen(now)
            return

        if self.lastlock == now.day:
            return

        if now.minute == 0 and now.hour == 0 and not init:
            self.setNewLastLock(now)
            return

        message = "Worktime end"
        self.setNewLastLock(now)

        if init:
            self.app.door.firstLock("Init outside of worktime", src="AUTOMATION")
        else:
            if not self.check_if_worktime(datetime.now().replace(microsecond=0)):
                self.app.door.lock(message, src="AUTOMATION")
            else:
                logging.warning(("Unexpected automation logic state"))
