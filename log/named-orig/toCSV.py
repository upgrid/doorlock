import csv
import re

csvfile = csv.DictReader(open('persons.csv', 'r'))

persons = {}

for line in csvfile:
    persons[line["uid"]] = line["name"]

# print(persons)


tags = {}


def collect_uids(name, tags):
    f = open(name)
    for line in f:
        l = line.strip().split("|")
        reason = l[2].split(":")[1]
        # print(reason)
        tag = ""
        if len(reason) == len(" Wiegand tag D7 2B 82"):
            tag = " ".join(reason.split(" ")[-3:])
        tags[tag] = 0
    f.close()


def write_file(name, tags):
    out = open("named-" + name, "a")

    f = open(name)
    for line in f:
        l = line.strip().split("|")
        reason = l[2].split(":")[1]
        # print(reason)
        if len(reason) == len(" Wiegand tag D7 2B 82"):
            tag = " ".join(reason.split(" ")[-3:])
            out.write(line.strip() + " " + tags[tag] + "\n")
        else:
            out.write(line)

    out.close()


files = ['3k-eesuks_uks.log', '3k-tpilet-1_uks.log', '3k-tpilet-2_uks.log', '3k-tpilet-3_uks.log',
         '3k-tpilet-juuksur_uks.log', '3k-tpilet-keerdtrepp_uks.log', '4k-eesuks_uks.log', '4k-pipedrive_uks.log']



for k in files:
    collect_uids("orig/" + k, tags)

for p in persons.keys():
    for tag in tags.keys():
        if tag:
            if re.search("^04 {0}*".format(tag), p) or re.search("^{0} *".format(tag), p) or re.search(
                    "^A7 {0}*".format(tag), p):
                if tags[tag] == 0:
                    tags[tag] = persons[p]
                else:
                    print("Duplicate person for tag", tag)

for k in files:
    write_file("orig/" + k, tags)