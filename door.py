import logging
import time
from datetime import datetime
from threading import Thread

from doorActionTypes import DoorActionTypes, DoorActionSrc


class Door:
    lastStateChangedSrc = ""

    def __init__(self, app, show_gpio_warnings=True):
        """
        :type app: LockApplicationMain
        """
        self.app = app
        if self.app.GPIO:
            self.pin_init(self.app.settings.DOOR_PIN, show_gpio_warnings)
            if self.app.settings.INVERTED_LED:
                self.pin_init(self.app.settings.LED_PIN, show_gpio_warnings)
        self.uid = self.app.settings.door_id

        self.opentime = 5
        self.pin_state = self.app.settings.LOCKED_PIN_STATE
        self.thread = None
        self.block_lock_with_timeot = False
        self.isFirstCommandCalled = False

    def __str__(self):
        return self.uid

    def pin_init(self, pin, show_gpio_warnings=True):
        self.app.GPIO.setwarnings(show_gpio_warnings)
        self.app.GPIO.setmode(self.app.GPIO.BCM)
        self.app.GPIO.setup(pin, self.app.GPIO.OUT)

    def get_pin_state(self):
        if self.app.GPIO:
            self.pin_state = self.app.GPIO.input(self.app.settings.DOOR_PIN)
        else:
            f = open(".states/lock_pin_state", "r")
            from_file = f.read().strip()
            self.pin_state = from_file == "True"
            f.close()
        return self.pin_state

    def isLocked(self):
        return self.get_pin_state() == self.app.settings.LOCKED_PIN_STATE

    def get_lock_state(self):
        state = "unlocked"
        if self.isLocked():
            state = "locked"
        return state

    def set_pin_state(self, new_state, force=False, src=""):
        if self.get_pin_state() == new_state and not force:
            logging.info("Door has already right state")
            self.app.sync.sendLive(
                locked=new_state,
                person=self.app.access.lastPersonWithTag.id,
                tag=self.app.access.lastPersonWithTag.tag.id,
                type=DoorActionTypes.LOCK_HAS_RIGHT_STATE,
                src=src,
            )
            return False

        self.lastStateChangedSrc = src
        if self.app.GPIO:
            self.app.GPIO.output(self.app.settings.DOOR_PIN, new_state)

            if self.app.settings.INVERTED_LED:
                self.app.GPIO.output(self.app.settings.LED_PIN, not new_state)
        else:
            f = open(".states/lock_pin_state", "w")
            f.write(str(new_state))
            f.close()

        return True

    def lock(self, reason, force=False, src=""):
        logging.info("Locking door: {0}".format(reason))
        self.set_pin_state(self.app.settings.LOCKED_PIN_STATE, force, src=src)

    def lock_with_timeot(self):
        time.sleep(self.opentime)
        if (
            self.app.settings.USE_AUTOMATIC_OPENING
            and self.app.automation
            and self.app.automation.check_if_worktime(datetime.now())
        ):
            logging.info("Door closing with timeout blocked by on going worktime")
            self.app.sync.sendLive(
                locked=None,
                message="Door locking blocked by on going worktime",
                type=DoorActionTypes.INFO,
                src=DoorActionSrc.AUTOLOCK,
            )
        elif self.app.automation and self.app.remoteOpenEndTime:
            logging.info("Door closing with timeout by ongoing remote open with time")
            self.app.sync.sendLive(
                locked=None,
                message="Door locking blocked by ongoing remote open until",
                type=DoorActionTypes.INFO,
                src=DoorActionSrc.AUTOLOCK,
            )
        elif self.block_lock_with_timeot:
            logging.debug("Door locking was blocked by internal boolean")
        else:
            self.lock("Door close", src=DoorActionSrc.AUTOLOCK)

        self.block_lock_with_timeot = False

    def unlock(self, reason, autolock=True, force=False, name="", idcode="", src=""):
        if self.thread and force:
            self.block_lock_with_timeot = True

        if not name:
            name = ""
        if not idcode:
            idcode = ""
        logging.info("Unlocking door: {0} {1} {2}".format(reason, name, idcode))

        if (
            self.set_pin_state(
                not self.app.settings.LOCKED_PIN_STATE, force, src=src
            )
            and autolock
        ):
            if False:
                logging.debug("Setting lock timeout")
                self.thread = Thread(target=self.lock_with_timeot)
                self.thread.start()
            else:
                self.lock_with_timeot()

    def firstLock(self, *args, **kwargs):
        if not self.isFirstCommandCalled:
            self.isFirstCommandCalled = True
            return self.lock(*args, **kwargs)

    def firstUnlock(self, *args, **kwargs):
        if not self.isFirstCommandCalled:
            self.isFirstCommandCalled = True
            return self.unlock(*args, **kwargs)
