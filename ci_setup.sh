#!/usr/bin/env bash

apt-get update

# Install deps from package manager
apt-get install -y pcscd opensc python3-pip sqlite3 pcsc-tools swig libpcsclite-dev python3-setuptools

pip3 install --upgrade argparse
pip install -r pip-versions.txt