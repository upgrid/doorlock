from behave import *

import settings
from access_controller import AccessController

use_step_matcher("re")





@given("I insert (?P<card>.+) into reader")
def step_impl(context, card):
    """
    Args:
        context (behave.runner.Context): 
        card (str): 
    """
    settings.ADD_UNKNOWN_IDCARD_TO_DATABASE = False
    settings.ADD_UNKNOWN_TAG_TO_DATABASE = False
    settings.sqlite_file = 'web/pytest.db.sqlite3'
    access = AccessController(settings)
    access.document_nr = card
    context.LOCKINFO, context.READINFO = access.accept_id()


@step("cardreader should read additional info (?P<readInfo>.+)")
def step_impl(context, readInfo):
    """
    Args:
        context (behave.runner.Context): 
        readInfo (str): 
    """
    assert str(context.READINFO) == readInfo


@then("I should get into door \((?P<response>.+)\)")
def step_impl(context, response):
    """
    Args:
        context (behave.runner.Context): 
        response (str): 
    """
    assert str(context.LOCKINFO) == response