Feature: Idcard insert
    I can insert idcard and check Documentcode

Scenario Outline: Cards
   Given I insert <card> into reader
   Then I should get into door (<response>)
   And  cardreader should read additional info (<readInfo>)

 Examples: Cards
   | card        | response | readInfo |
   |A234123412  | False    | True     |
   |AA0825372   | True     | False    |
