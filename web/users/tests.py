import json
from django.urls import reverse
from django.test import TestCase

class UsersViewTest(TestCase):
    def openWithTime(self,token="DEV", timestamp=0):
        """
        If no questions exist, an appropriate message is displayed.
        """
        data = {
            "token" : token,
            "name" : "Test",
            "timestamp" : timestamp
         }
        response = self.client.post(reverse('openwithtime'), json.dumps(data), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        return response

    def closeWithTime(self, token="DEV"):
        """
        If no questions exist, an appropriate message is displayed.
        """
        data = {
            "token" : token,
            "name" : "Test",
        }
        response = self.client.post(reverse('closewithtime'), json.dumps(data), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        return response


    def test_closeWithTime(self):
        openWithTimeReq = self.openWithTime(timestamp=1601712188)
        self.assertContains(openWithTimeReq, "Door will be open until")
        closeWithTimeReq = self.closeWithTime()
        try:
            self.assertContains(closeWithTimeReq, "Remote closing disallowed: active worktime")
        except Exception:
            self.assertContains(closeWithTimeReq, "Door closed")



    def test_invalidTokenOpenClose(self):
        openWithTimeReq = self.openWithTime(token="INVALID")
        self.assertContains(openWithTimeReq, "Invalid token")
        closeWithTimeReq = self.closeWithTime(token="INVALID")
        self.assertContains(closeWithTimeReq, "Invalid token")

    def test_invalidTimestampOpen(self):
        openWithTimeReq = self.openWithTime(timestamp=0)
        self.assertContains(openWithTimeReq, "Disallowed timestamp")
