from django.contrib import admin

from .models import IDCard, Tag, Person, Group, GroupLimit


@admin.action(
    description="Activate selected items"
)
def activate_item(modeladmin, request, queryset):
    queryset.update(active=True)


@admin.action(
    description="Deactivate selected items"
)
def deactivate_item(modeladmin, request, queryset):
    queryset.update(active=False)




class IDCardInline(admin.TabularInline):
    """Inline configuration for Django's admin on the IDCard model."""
    model = IDCard
    extra = 1

    def get_extra(self, request, obj=None, **kwargs):
        """Dynamically sets the number of extra forms. 0 if the related object
        already exists or the extra configuration otherwise."""
        if obj:
            # Don't add any extra forms if the related object already exists.
            return 0
        return self.extra


class TagInline(admin.TabularInline):
    """Inline configuration for Django's admin on the Tag model."""
    model = Tag
    extra = 2

    def get_extra(self, request, obj=None, **kwargs):
        """Dynamically sets the number of extra forms. 0 if the related object
        already exists or the extra configuration otherwise."""
        if obj:
            # Don't add any extra forms if the related object already exists.
            return 0
        return self.extra


class MembershipInline(admin.TabularInline):
    """Inline configuration for Django's admin on the Group.members model."""
    model = Group.members.through
    extra = 2

    def get_extra(self, request, obj=None, **kwargs):
        """Dynamically sets the number of extra forms. 0 if the related object
        already exists or the extra configuration otherwise."""
        if obj:
            # Don't add any extra forms if the related object already exists.
            return 0
        return self.extra


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ( "id", 'name', 'idcode', 'active', 'unactive_until')
    search_fields = ("id", 'name', 'idcode', 'active', 'unactive_until')
    list_filter = ('active','unactive_until')
    inlines = [MembershipInline, IDCardInline, TagInline]
    actions = (activate_item, deactivate_item)


@admin.register(IDCard)
class IDCardAdmin(admin.ModelAdmin):
    list_display = ("id", 'document', 'person', "active", "added", "updated")
    search_fields = ("id", 'person__idcode', 'person__name', 'document')
    list_filter = ("active", "added", "updated")
    readonly_fields = ("added", "updated")
    actions = (activate_item, deactivate_item)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ("id", 'uid', 'name', "person", "active", "added", "updated")
    search_fields = ("id", 'uid', 'name', "person__name")
    list_filter = ("active", "added", "updated")
    readonly_fields = ("added", "updated")
    actions = (activate_item, deactivate_item)

@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("id", 'name',)
    search_fields = ("id", 'name',)
    list_filter = ("added", "updated")
    readonly_fields = ("added", "updated")
    inlines = [
        MembershipInline,
    ]
    exclude = ('members',)

@admin.register(GroupLimit)
class GroupLimitAdmin(admin.ModelAdmin):
    list_display = ("id", "group", "fromTime", "toTime", "weekday")
    search_fields = ("id", "group__name", "fromTime", "toTime", "weekday")
    list_filter = ("weekday",)
    readonly_fields = ("added", "updated")

