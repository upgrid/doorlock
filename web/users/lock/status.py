#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import os
import sys

import settings

sys.path.append(os.path.join(os.path.dirname(__file__), "."))

logging.basicConfig(
    filename = 'uks.log',
    level = logging.INFO,
    format = '%(asctime)s | %(levelname)s | %(message)s'
)

if __name__ == '__main__':
    from main import LockApplicationMain

    settings.sqlite_file = "./db.sqlite3"
    app = LockApplicationMain(settings, full=False)
    print(app.door.get_lock_state())
