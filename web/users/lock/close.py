#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
import logging
import os
import sys

import settings

sys.path.append(os.path.join(os.path.dirname(__file__), "."))

try:
    import RPi.GPIO as GPIO
except ImportError:
    GPIO = False

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('name')
args = parser.parse_args()

logging.basicConfig(
    filename='uks.log',
    level=logging.INFO,
    format='%(asctime)s | %(levelname)s | %(message)s'
)

if __name__ == '__main__':
    from main import LockApplicationMain

    settings.sqlite_file = "./db.sqlite3"
    app = LockApplicationMain(settings, full=False)
    app.door.opentime = 10
    reason = "remote close"
    if args.name:
        reason += " by " + args.name
    app.door.lock(reason=reason, src="REMOTE")
    print(app.door.get_lock_state())
