#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
import logging
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "."))

import settings

try:
    import RPi.GPIO as GPIO
except ImportError:
    GPIO = False

logging.basicConfig(
    filename='uks.log',
    level=logging.INFO,
    format='%(asctime)s | %(levelname)s | %(message)s'
)

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('name')
args = parser.parse_args()

if __name__ == '__main__':
    settings.sqlite_file = "./db.sqlite3"
    settings.LOCK_OPEN_TIME = 5
    settings.ALLOW_STATUS_SYNC = True
    from main import LockApplicationMain

    app = LockApplicationMain(settings, full=False)
    app.door.unlock(reason="remote open by " + args.name, autolock=True, src="REMOTE")

    print(app.door.get_lock_state())
