# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0043_merge_20180710_2132'),
    ]

    operations = [
        migrations.AddField(
            model_name='doorlog',
            name='uid',
            field=models.CharField(max_length=41, null=True, db_index=True),
        ),
    ]
