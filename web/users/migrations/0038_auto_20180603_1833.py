# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-06-03 15:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0037_auto_20180603_1715'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grouplimit',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='users.Group'),
        ),
    ]
