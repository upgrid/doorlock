# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-27 11:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20160327_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nfc',
            name='uid',
            field=models.CharField(editable=False, max_length=41, unique=True),
        ),
    ]
