# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2019-02-04 16:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0045_auto_20190204_1822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doorlog',
            name='type',
            field=models.CharField(db_index=True, default='INFO', max_length=30, null=True),
        ),
    ]
