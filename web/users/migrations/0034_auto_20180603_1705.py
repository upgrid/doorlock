# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-06-03 14:05
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0033_auto_20180603_1640'),
    ]

    operations = [
        migrations.RenameField(
            model_name='grouplimit',
            old_name='from_time',
            new_name='fromTime',
        ),
        migrations.RenameField(
            model_name='grouplimit',
            old_name='to_time',
            new_name='toTime',
        ),
        migrations.RemoveField(
            model_name='person',
            name='group',
        ),
        migrations.AddField(
            model_name='group',
            name='members',
            field=models.ManyToManyField(related_name='groups', to='users.Person'),
        ),
        migrations.AddField(
            model_name='grouplimit',
            name='added',
            field=models.DateTimeField(auto_now_add=True, db_index=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='grouplimit',
            name='updated',
            field=models.DateTimeField(auto_now=True, db_index=True),
        ),
    ]
