from django.core.validators import MaxValueValidator, MinValueValidator

from django.db import models


# Create your models here.
class Person(models.Model):
    name = models.CharField(db_index=True, max_length=50, null=True, blank=True,)
    idcode = models.CharField(db_index=True, max_length=11, null=True, blank=True)
    active = models.BooleanField(db_index=True, default=True)
    unactive_until = models.DateTimeField(null=True, blank=True)
    added = models.DateTimeField(db_index=True, auto_now_add=True)
    updated = models.DateTimeField(db_index=True, auto_now=True)


    def __str__(self):
        return "{0} | {1}".format(self.id, self.name)


class IDCard(models.Model):
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        null=True, blank=True,
        db_index=True
    )
    document = models.CharField(db_index=True, max_length=9, unique=True)
    active = models.BooleanField(db_index=True, default=True)
    added = models.DateTimeField(db_index=True, auto_now_add=True)
    updated = models.DateTimeField(db_index=True, auto_now=True)

    def save(self, *args, **kwargs):
        self.document = self.document.upper()
        super(IDCard, self).save(*args, **kwargs)

    def __str__(self):
        return self.document


class Tag(models.Model):
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        null=True, blank=True,
        db_index=True
    )
    uid = models.CharField(db_index=True, max_length=41, unique=True)
    name = models.CharField(db_index=True, max_length=50, null=True, blank=True)
    active = models.BooleanField(db_index=True, default=True)
    added = models.DateTimeField(db_index=True, auto_now_add=True)
    updated = models.DateTimeField(db_index=True, auto_now=True)

    def save(self, *args, **kwargs):
        self.uid = self.uid.upper()
        super(Tag, self).save(*args, **kwargs)

    def __str__(self):
        return self.uid


class Group(models.Model):
    name = models.CharField(db_index=True, max_length=50, null=True, blank=True)
    members = models.ManyToManyField(Person, related_name='groups')
    added = models.DateTimeField(db_index=True, auto_now_add=True)
    updated = models.DateTimeField(db_index=True, auto_now=True)

    def __str__(self):
        return "{0} | {1}".format(self.id, self.name)


class GroupLimit(models.Model):
    WEEKDAYS = (
        (0, 'Sunday'),
        (1, 'Monday'),
        (2, 'Tuesday'),
        (3, 'Wednesday'),
        (4, 'Thursday'),
        (5, 'Friday'),
        (6, 'Saturday')
    )

    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        null=True,
        db_index=True,
    )
    fromTime = models.TimeField(db_index=True, null=True, blank=True, db_column="from_time")
    toTime = models.TimeField(db_index=True, null=True, blank=True, db_column="to_time")
    weekday = models.IntegerField(db_index=True, null=False, validators=[MaxValueValidator(7), MinValueValidator(0)], choices=WEEKDAYS)
    added = models.DateTimeField(db_index=True, auto_now_add=True)
    updated = models.DateTimeField(db_index=True, auto_now=True)



    def __str__(self):
        return "{0} | {1} - {2}".format(self.weekday, self.fromTime, self.toTime)


class DoorLog(models.Model):
    person_id = models.IntegerField(
        null=True, blank=True,
        db_index=True
    )

    tag_id = models.IntegerField(
        null=True, blank=True,
        db_index=True
    )

    idcard_id = models.IntegerField(
        null=True, blank=True,
        db_index=True
    )

    locked = models.BooleanField(null=True, blank=True)

    added = models.DateTimeField(db_index=True, auto_now_add=True)
    eventDate = models.DateTimeField(db_index=True)
    message = models.CharField(max_length=100, null=True, blank=True)
    uid = models.CharField(db_index=True, max_length=41, null=True)
    type = models.CharField(null=True, db_index=True, max_length=30, default="INFO")
    src = models.CharField(null=True, db_index=True, max_length=30, default=None)