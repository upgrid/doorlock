from django.urls import path

from . import views

urlpatterns = [
    # ex: /users/
    path('close/', views.doorClose, name='doorClose'),
    path('open/', views.doorOpen, name='doorOpen'),
    path('openwithtime/', views.openDoorWithTime, name='openwithtime'),
    path('closewithtime/', views.closeDoorWithTime, name='closewithtime'),
    path('status/', views.getStatus, name='doorStatus'),
    path('update/tags/<int:timestamp>', views.setTagsUpdateMarker, name='tags_update_marker'),
    path('update/grouplimits/<int:timestamp>', views.setGroupLimitMarker, name='grouplimits_update_marker'),
]