import json
import shlex
from datetime import datetime
from time import time

import os
import threading

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

import subprocess
from .lock import settings as doorlock_settings
from .lock.main import LockApplicationMain
import logging

logger = logging.getLogger(__name__)
def getLockApplication(settings):
    settings.sqlite_file = "./db.sqlite3"
    lockApplication = LockApplicationMain(settings, full = False)
    return lockApplication

def sub_door_open(name):
    os.system("python3 ./lock/open.py %s" %(shlex.quote(name)))

@csrf_exempt
def doorOpen(request):
    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        if doorlock_settings.ALLOW_REMOTE_OPEN:
            if doorlock_settings.TOKEN == body['token']:
                t = threading.Thread(target=sub_door_open, args=(), kwargs={"name": body['name']}, daemon=True)
                t.start()
                return HttpResponse("Opening door for 5 seconds")
            else:
                return HttpResponse("Invalid token")
        return HttpResponse("Remote opening is not allowed.")

def getStatus(request):
    p = subprocess.Popen(
        ['python3', './lock/status.py'],
        stdout=subprocess.PIPE
    )
    app = getLockApplication(doorlock_settings)
    newState = app.door.get_lock_state()
    if newState in ["locked", "unlocked"]:
        return HttpResponse(json.dumps({
            "newState" : newState,
            "openUntil" : None,
            "error" : None
        }))
    else:
        return HttpResponse(json.dumps({
            "newState" : None,
            "openUntil" : None,
            "error" : "runtime error"
        }))

def doorClose(request):
    if doorlock_settings.ALLOW_REMOTE_CLOSE:
        p = subprocess.Popen(
            ['python3', './lock/close.py'],
            stdout=subprocess.PIPE
        )
        return HttpResponse(p.stdout.read())
    return HttpResponse("Door remote close is not allowed.")

def setTagsUpdateMarker(request, timestamp):
    if len(str(timestamp)) >= 14:
        return HttpResponse("To long timestamp")
    f = open(doorlock_settings.UPDATE_MARKER_FILE, "r")
    r = f.read().strip()
    f.close()
    if not r:
        f = open(doorlock_settings.UPDATE_MARKER_FILE, "w")
        f.write(str(timestamp))
        f.close()
        return HttpResponse("Marker set.")
    return HttpResponse("Marker was already in place")

def setGroupLimitMarker(request, timestamp):
    if len(str(timestamp)) >= 14:
        return HttpResponse("To long timestamp")
    f = open("./.states/updateGroupLimits", "r")
    r = f.read().strip()
    f.close()
    if not r:
        f = open("./.states/updateGroupLimits", "w")
        f.write(str(timestamp))
        f.close()
        return HttpResponse("Marker set.")
    return HttpResponse("Marker was already in place")

@csrf_exempt
def openDoorWithTime(request):
    if request.method == "POST":
        failedState = "locked"
        body = json.loads(request.body.decode("utf-8"))
        if doorlock_settings.TOKEN == body['token']:
            if doorlock_settings.ALLOW_REMOTE_OPEN_WITH_TIME:
                timestamp = str(int(time()) + int(body['openTime']))
                dateTimeObj = datetime.fromtimestamp(int(timestamp))
                app = getLockApplication(doorlock_settings)
                if app.automation.check_if_worktime(dateTimeObj):
                    return HttpResponse(json.dumps({
                        "newState" : failedState,
                        "openUntil" : None,
                        "warning" : "You can't close door during workday"
                    }))
                if len(str(timestamp)) >= 14:
                    return HttpResponse(json.dumps({
                        "newState" : failedState,
                        "openUntil" : str(datetime.fromtimestamp(int(timestamp))),
                        "error" : "To long timestamp"
                    }))
                f = open("./.states/remoteOpen", "w")
                f.write(timestamp)
                f.close()
                p = subprocess.Popen(
                    ['python3', 'lock/open.py', "%s" %(shlex.quote(body['name']))],
                    stdout=subprocess.PIPE
                )
                if p.stdout.read().strip() == b"unlocked":
                    return HttpResponse(json.dumps({
                        "newState" : "unlocked",
                        "openUntil" : str(datetime.fromtimestamp(int(timestamp))),
                        "error" : None
                    }))
                else:
                    f = open(doorlock_settings.REMOTE_OPEN_FILE, "w")
                    f.write("")
                    f.close()
                    return HttpResponse(json.dumps({
                        "newState" : failedState,
                        "openUntil" : None,
                        "error" : "runtime error"
                    }))
            else:
                return HttpResponse(json.dumps({
                    "newState" : failedState,
                    "openUntil" : None,
                    "error" : "Remote open with time is now allowed"
                }))
        return HttpResponse(json.dumps({
            "newState" : failedState,
            "openUntil" : None,
            "error" : "Invalid token"
        }))

@csrf_exempt
def closeDoorWithTime(request):
    if request.method == "POST":
        failedState = "unlocked"
        body = json.loads(request.body.decode("utf-8"))
        if not doorlock_settings.ALLOW_REMOTE_OPEN_WITH_TIME:
            return HttpResponse(json.dumps({
                "newState" : failedState,
                "openUntil" : None,
                "error" : "Remote open with time is no allowed"
            }))

        if doorlock_settings.TOKEN != body['token']:
            return HttpResponse(json.dumps({
                "newState" : failedState,
                "openUntil" : None,
                "error" : "Invalid token"
            }))

        f = open(doorlock_settings.REMOTE_OPEN_FILE, "r")
        r = f.read().strip()
        f.close()
        if not r:
            return HttpResponse(json.dumps({
                "newState" : failedState,
                "openUntil" : None,
                "warning" : "No active open until date."
            }))
        f = open(doorlock_settings.REMOTE_OPEN_FILE, "w")
        f.write("")
        f.close()
        app = getLockApplication(doorlock_settings)
        now = datetime.now()

        if app.automation.check_if_worktime(now):
            return HttpResponse(json.dumps({
                "newState" : "locked",
                "openUntil" : None,
                "message" : "Open until cleared. Door kept open"
            }))
        else:
            p = subprocess.Popen(
                ['python3', 'lock/close.py', "%s" %(shlex.quote(body['name']))],
                stdout=subprocess.PIPE
            )
            if p.stdout.read().strip() == b"locked":
                return HttpResponse(json.dumps({
                    "newState" : "locked",
                    "openUntil" : None,
                    "error" : None
                }))
            else:
                return HttpResponse(json.dumps({
                    "newState" : failedState,
                    "openUntil" : None,
                    "error" : "Door closing failed: runtime error"
                }))