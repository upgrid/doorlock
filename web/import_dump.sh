#!/usr/bin/env bash

mv db.sqlite3 backups/db_$(date +%s).sqlite3 ; sqlite3 db.sqlite3 ".read dumps/dump.sql"