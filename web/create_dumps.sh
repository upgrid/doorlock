#!/bin/bash

DUMP_FOLDER="dumps"
#Create users table dump:
#python3 manage.py dumpdata users --indent 4 > $DUMP_FOLDER/users.json

# Full database dump
sqlite3 db.sqlite3 .dump > $DUMP_FOLDER/dump.sql
grep -v "INSERT INTO \"django_session\"\|INSERT INTO \"django_admin_log\"\|INSERT INTO \"users_tag\"\|INSERT INTO \"users_idcard\"\|INSERT INTO \"users_person\"" $DUMP_FOLDER/dump.sql  -h > $DUMP_FOLDER/clean.sql
