#!/usr/bin/env bash

cp db.sqlite3 backups/db_$(date +%s).sqlite3

sqlite3 db.sqlite3 "delete from users_doorlog;"

sqlite3 db.sqlite3 "delete from users_group_members;"

sqlite3 db.sqlite3 "delete from users_grouplimit;"

sqlite3 db.sqlite3 "delete from users_group;"

sqlite3 db.sqlite3 "delete from users_idcard;"

sqlite3 db.sqlite3 "delete from users_tag;"

sqlite3 db.sqlite3 "delete from users_person;"

echo 1 > ../.states/updateMarker

