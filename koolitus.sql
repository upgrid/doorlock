SET @ALLOWED_ZONE_ID = 58;
SET @DOOR_ID = 4;
SET @NOW = CURRENT_TIMESTAMP;
UPDATE doorzones SET revoked_date = CURRENT_TIMESTAMP where door_id = @DOOR_ID AND revoked_date IS NULL AND zone_id != @ALLOWED_ZONE_ID;
INSERT INTO doorsync (door_id, sync_from) VALUE (@DOOR_ID, @NOW);
